<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dao.HotelDAO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.File" %>
<%@ page import="com.dto.Hotel" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		HotelDAO hdo = HotelDAO.getInstance();
		ArrayList<Hotel> list = hdo.getHotelList(null, null, null, null, null, null, 1, 5);
		String imgPath=request.getContextPath()+"/images/q.gif";
		ArrayList<String> imgPaths=new ArrayList<String>();
		for (int i=0;i<list.size();i++){
			imgPaths.add(imgPath);
			File dir = new File(request.getRealPath(File.separator)+"/images/hotel/"+list.get(i).getH_idx()+"/");
			if(!dir.isDirectory()){System.out.print("폴더없음");}
			else{
				File[] fList = dir.listFiles();
				if(fList.length>0){
					String pic = request.getContextPath()+"/images/hotel/"+list.get(i).getH_idx()+"/"+fList[0].getName();
					imgPaths.add(i, pic);
				}
			}
		}
	%>
			<h2>최고 평점을 받은 숙소</h2>
			<h4>최고의 평점을 받은 한국의 숙소를 둘러보세요.</h4>
			<%for (int i=0; i < 5; i++){ %>
			<div id="<%=list.get(i).getH_idx() %>" class="recom best">
				<img src="<%=imgPaths.get(i) %>" id="best1" alt="best<%=i%>">
				<p><span><%=list.get(i).getH_type2() %> / <%=list.get(i).getH_address1() %></span> <br><%=list.get(i).getH_title() %></p><span><%=list.get(i).getH_star() %></span>
			</div>
			<%} %>
</body>
<script>
	$('.best').click(function(){
		location.href="DetailInfo?h_idx="+$(this).attr('id');
	});
</script>
</html>