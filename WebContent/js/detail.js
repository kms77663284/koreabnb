			function reviewdelete(r_idx) {
		if(confirm('정말 삭제 하겠습니까?')){
			location.href='reviewDelete?r_idx='+r_idx+'&h_idx='+h_idx;
		}else{
			return false;
		}
		
	}

function reserveSend(guests,re1) {
			var frm = document.reservefrm;
			var today;
			
			var g = $('select[name=ppl]').val();
			var start =  $('input[name=checkin]').val();
			var end =  $('input[name=checkout]').val();
			
			if(g>guests){
				alert('숙박인원을 확인해주세요.');
				return false;
			}
			$(function() {
				today= getTodayType2();
				if(start ==""){
					alert("체크인을 입력해주세요.");
					return false;
				}
				if(end == ""){
					alert("체크아웃을 입력해주세요.");
					return false;
				}
				if(start < today && start != ""){
					alert("체크인이 오늘 이전입니다. 확인해주세요.");
					return false;
				}
				var y = start.substr(0,4);
				var m = start.substr(5,2);
			    var d = start.substr(8,2); 
			    var exdate = new Date(y, m-1, d);
				var startdate = getTypechange(exdate);
				if(startdate >= end){
					alert("체크인과 체크아웃을 확인해주세요.");
					return false;
					
				}
				var datelist = re1.split(',');
				
				for ( var i in datelist ) {
					var yyyy = datelist[i].substr(0,4);
				    var mm = datelist[i].substr(5,2);
				    var dd = datelist[i].substr(8,2);   

					var date = new Date(yyyy, mm-1, dd);
					var date = getTypechange(date);
					
					if(date > start && date < end ){
						alert("예약 가능 날짜를 확인하세요.");
						return false;
					}
				}
				frm.submit();
			});
			
			
		}
		function getTypechange(date){
			return date.getFullYear() + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + ("0"+date.getDate()).slice(-2);
		}
		function getTodayType2() {
			var date = new Date();
			return date.getFullYear() + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + ("0"+date.getDate()).slice(-2);
		}
	
		bookmark2 = function(user) {
			var frm = document.bookmark1;
			
			
			if(user== null){
				alert('로그인해주세요');
				location.href="Login.jsp";
				return false;
			}

			frm.submit();
		}
		$(function () {
			$('.input-group.date').datepicker({
				calendarWeeks: false, todayHighlight: true, autoclose: true, //사용자가 날짜를 클릭하면 자동 캘린더가 닫히는 옵션
				datesDisabled: [], //선택 불가능한 일 설정 하는 배열 위에 있는 format 과 형식이 같아야함.
				format: "yyyy-mm-dd",
				multidate: false, //여러 날짜 선택할 수 있게 하는 옵션 기본값 :false
				multidateSeparator: ",", //여러 날짜를 선택했을 때 사이에 나타나는 글짜 2019-05-01,2019-06-01
				language: "ko"
			});
			
			});

		



$(document).ready(function () {
    function c(passed_month, passed_year, calNum) {
        var calendar = calNum == 0 ? calendars.cal1 : calendars.cal2;
        makeWeek(calendar.weekline); // sun, mon, tue, wed, thu, fri, sat
        calendar.datesBody.empty(); // content delete
        var calMonthArray = makeMonthArray(passed_month, passed_year); // {day:1, weekday:fri}, {day:2, weekday:sat},...
        var r = 0;
        var u = false;
        while (!u) {
            if (daysArray[r] == calMonthArray[0].weekday) {
                u = true
            } else {
                calendar.datesBody.append('<div class="blank"></div>');
                r++;
            }
        } // blank 삽입 ㅁ ㅁ ㅁ ㅁ ㅁ 1 2
            //          3  4  5  6  ..

        for (var cell = 0; cell < 42 - r; cell++) { // 42 date-cells in calendar
        	
        	var d = parseInt(res_days(res_day));
            if (cell >= calMonthArray.length) {
                calendar.datesBody.append('<div class="blank"></div>'); // 30일보다 많을 때 blank 처리
            }   else {
                var shownDate = calMonthArray[cell].day;
                var iter_date = new Date(passed_year, passed_month, shownDate);
                
                if (((shownDate != today.getDate() && passed_month == today.getMonth()) || passed_month != today.getMonth()) && iter_date < today) {
                    var m = '<div class="past-date">'; // 1 ~ 14
                } 
                else {
                    var m = checkToday(iter_date) ? '<div class="today">' : "<div>"; // 15 : 16 ~ 30
                }
                if(shownDate == d){
                	for(var j = 0; j < res_day.length; j++){
                		res_day[j] = res_day[j+1];
                	}
                	var m = '<div class="past-date">';
                }
                
                
                calendar.datesBody.append(m + shownDate + "</div>");
            }
        }

        var color = "#444444";
        calendar.calHeader.find("h2").text(passed_year + " " + i[passed_month]); // 2019 11월
        calendar.weekline.find("div").css("color", color);

        // find elements (dates) to be clicked on each time
        // the calendar is generated
        var clicked = false;
        selectDates(selected); // sselected가 객체인지 여부 

        clickedElement = calendar.datesBody.find('div'); // datesBody에 div태그선택
        clickedElement.on("click", function () {
            clicked = $(this);
            var whichCalendar = calendar.name;

            if (firstClick && secondClick) {
                thirdClicked = getClickedInfo(clicked, calendar);
                var firstClickDateObj = new Date(firstClicked.year,
                firstClicked.month,
                firstClicked.date);
                var secondClickDateObj = new Date(secondClicked.year,
                secondClicked.month,
                secondClicked.date);
                var thirdClickDateObj = new Date(thirdClicked.year,
                thirdClicked.month,
                thirdClicked.date);
                if (secondClickDateObj > thirdClickDateObj && thirdClickDateObj > firstClickDateObj) {
                    secondClicked = thirdClicked;
                    // then choose dates again from the start :)
                    bothCals.find(".calendar_content").find("div").each(function () {
                        $(this).removeClass("selected");
                    });
                    selected = {};
                    selected[firstClicked.year] = {};
                    selected[firstClicked.year][firstClicked.month] = [firstClicked.date];
                    selected = addChosenDates(firstClicked, secondClicked, selected);
                } else { // reset clicks
                    selected = {};
                    firstClicked = [];
                    secondClicked = [];
                    firstClick = false;
                    secondClick = false;
                    bothCals.find(".calendar_content").find("div").each(function () {
                        $(this).removeClass("selected");
                    });
                }
            }
            if (!firstClick) {
                firstClick = true;
                firstClicked = getClickedInfo(clicked, calendar);
                selected[firstClicked.year] = {};
                selected[firstClicked.year][firstClicked.month] = [firstClicked.date];
                document.getElementById("checkin").value = year+'-'+(month+1)+'-'+ firstClicked.date;
                
                
            } else {
                secondClick = true;
                secondClicked = getClickedInfo(clicked, calendar);

                // what if second clicked date is before the first clicked?
                var firstClickDateObj = new Date(firstClicked.year,
                firstClicked.month,
                firstClicked.date);
                var secondClickDateObj = new Date(secondClicked.year,
                secondClicked.month,
                secondClicked.date);

                if (firstClickDateObj > secondClickDateObj) {

                    var cachedClickedInfo = secondClicked;
                    secondClicked = firstClicked;
                    firstClicked = cachedClickedInfo;
                    selected = {};
                    selected[firstClicked.year] = {};
                    selected[firstClicked.year][firstClicked.month] = [firstClicked.date];

                } else if (firstClickDateObj.getTime() == secondClickDateObj.getTime()) {
                    selected = {};
                    firstClicked = [];
                    secondClicked = [];
                    firstClick = false;
                    secondClick = false;
                    $(this).removeClass("selected");
                }


                // add between dates to [selected]
                selected = addChosenDates(firstClicked, secondClicked, selected);
                document.getElementById("checkout").value = year+'-'+(month+1)+'-'+secondClicked.date;
            }
            selectDates(selected);
        });

    }

    function selectDates(selected) {
        if (!$.isEmptyObject(selected)) {
            var dateElements1 = datesBody1.find('div');
            var dateElements2 = datesBody2.find('div');

            function highlightDates(passed_year, passed_month, dateElements) {
                if (passed_year in selected && passed_month in selected[passed_year]) {
                    var daysToCompare = selected[passed_year][passed_month];
                    for (var d in daysToCompare) {
                        dateElements.each(function (index) {
                            if (parseInt($(this).text()) == daysToCompare[d]) {
                                $(this).addClass('selected');
                            }
                        });
                    }

                }
            }
          document.getElementById("checkin").value = year+'-'+(month+1)+'-'+ firstClicked.date;
          document.getElementById("checkout").value = year+'-'+(month+1)+'-'+ secondClicked.date;
            
            highlightDates(year, month, dateElements1);
            highlightDates(nextYear, nextMonth, dateElements2);
        }
    }

    function makeMonthArray(passed_month, passed_year) { // creates Array specifying dates and weekdays
        var e = [];
        for (var r = 1; r < getDaysInMonth(passed_year, passed_month) + 1; r++) {
            e.push({
                day: r,
                // Later refactor -- weekday needed only for first week
                weekday: daysArray[getWeekdayNum(passed_year, passed_month, r)]
            });
        }
        return e;
    }

    function makeWeek(week) {
        week.empty();
        for (var e = 0; e < 7; e++) {
            week.append("<div>" + daysArray[e].substring(0, 3) + "</div>")
        }
    }

    function getDaysInMonth(currentYear, currentMon) {
        return (new Date(currentYear, currentMon + 1, 0)).getDate();
    }

    function getWeekdayNum(e, t, n) {
        return (new Date(e, t, n)).getDay();
    }

    function checkToday(e) {
        var todayDate = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var checkingDate = e.getFullYear() + '/' + (e.getMonth() + 1) + '/' + e.getDate();
        return todayDate == checkingDate;

    }

    function getAdjacentMonth(curr_month, curr_year, direction) {
        var theNextMonth;
        var theNextYear;
        if (direction == "next") {
            theNextMonth = (curr_month + 1) % 12;
            theNextYear = (curr_month == 11) ? curr_year + 1 : curr_year;
        } else {
            theNextMonth = (curr_month == 0) ? 11 : curr_month - 1;
            theNextYear = (curr_month == 0) ? curr_year - 1 : curr_year;
        }
        return [theNextMonth, theNextYear];
    }

    function b() {
        today = new Date;
        year = today.getFullYear();
        month = today.getMonth();

        var nextDates = getAdjacentMonth(month, year, "next");
        nextMonth = nextDates[0];
        nextYear = nextDates[1];
    }

    var e = 480;
    
    /*var res_not = $('#res_not').val();
    var dayList = [];
    function res_not_date(res_not){
		var e = [];
    	var blankCalendar = res_not.split(','); // 2019-11-18[0] 2019-11-19[1] ...
    	for ( var i in blankCalendar ) {
            var blankDate = blankCalendar[i].split('-'); // 2019-11-18[0] -> 2019,11,18
            dayList[i] = blankDate[2]; // 18, 19, 20, 29
          }
    	return dayList;
    }*/
    var res_not = $('#res_not').val();
    var res_date = res_not.split(',');
    var res_day=[]
    for(var i in res_date){
       res_day[i] = res_date[i].substr(8,2);
    }
    res_day.sort(function(a, b){
    	return a - b;
    })
    
    
    function res_days(res_arr){ // 01 02 넘어옴
       for(var i in res_arr){
           var result = res_arr[i];
          return result;
       }
    }
    
    
    
    var today;
    var year,
    month,
    nextMonth,
    nextYear;

    var r = [];
    var i = [
        "1월",
        "2월",
        "3월",
        "4월",
        "5월",
        "6월",
        "7월",
        "8월",
        "9월",
        "10월",
        "11월",
        "12월"];
    var daysArray = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"];

    var cal1 = $("#calendar_first");
    var calHeader1 = cal1.find(".calendar_header");
    var weekline1 = cal1.find(".calendar_weekdays");
    var datesBody1 = cal1.find(".calendar_content");

    var cal2 = $("#calendar_second");
    var calHeader2 = cal2.find(".calendar_header");
    var weekline2 = cal2.find(".calendar_weekdays");
    var datesBody2 = cal2.find(".calendar_content");

    var bothCals = $(".calendar");

    var switchButton = bothCals.find(".calendar_header").find('.switch-month');

    var calendars = {
        "cal1": {
            "name": "first",
                "calHeader": calHeader1,
                "weekline": weekline1,
                "datesBody": datesBody1
        },
        "cal2": {
            "name": "second",
                "calHeader": calHeader2,
                "weekline": weekline2,
                "datesBody": datesBody2
        }
    }


    var clickedElement;
    var firstClicked,
    secondClicked,
    thirdClicked;
    var firstClick = false;
    var secondClick = false;
    var selected = {};

    b();
    c(month, year, 0);
    c(nextMonth, nextYear, 1);
    switchButton.on("click", function () {
        var clicked = $(this);
        var generateCalendars = function (e) {
            var nextDatesFirst = getAdjacentMonth(month, year, e);
            var nextDatesSecond = getAdjacentMonth(nextMonth, nextYear, e);
            month = nextDatesFirst[0];
            year = nextDatesFirst[1];

            nextMonth = nextDatesSecond[0];
            nextYear = nextDatesSecond[1];

            c(month, year, 0);
            c(nextMonth, nextYear, 1);
        };
        if (clicked.attr("class").indexOf("left") != -1) {
            generateCalendars("previous");
        } else {
            generateCalendars("next");
        }
        clickedElement = bothCals.find(".calendar_content").find("div");
    });


    //  Click picking stuff
    function getClickedInfo(element, calendar) {
        var clickedInfo = {};
        var clickedCalendar,
        clickedMonth,
        clickedYear;
        clickedCalendar = calendar.name;
        clickedMonth = clickedCalendar == "first" ? month : nextMonth;
        clickedYear = clickedCalendar == "first" ? year : nextYear;
        clickedInfo = {
            "calNum": clickedCalendar,
                "date": parseInt(element.text()),
                "month": clickedMonth,
                "year": clickedYear
        }
        return clickedInfo;
    }


    // Finding between dates MADNESS. Needs refactoring and smartening up :)
    function addChosenDates(firstClicked, secondClicked, selected) {
        if (secondClicked.date > firstClicked.date || secondClicked.month > firstClicked.month || secondClicked.year > firstClicked.year) {

            var added_year = secondClicked.year;
            var added_month = secondClicked.month;
            var added_date = secondClicked.date;

            if (added_year > firstClicked.year) {
                // first add all dates from all months of Second-Clicked-Year
                selected[added_year] = {};
                selected[added_year][added_month] = [];
                for (var i = 1;
                i <= secondClicked.date;
                i++) {
                    selected[added_year][added_month].push(i);
                }

                added_month = added_month - 1;
                while (added_month >= 0) {
                    selected[added_year][added_month] = [];
                    for (var i = 1;
                    i <= getDaysInMonth(added_year, added_month);
                    i++) {
                        selected[added_year][added_month].push(i);
                    }
                    added_month = added_month - 1;
                }

                added_year = added_year - 1;
                added_month = 11; // reset month to Dec because we decreased year
                added_date = getDaysInMonth(added_year, added_month); // reset date as well

                // Now add all dates from all months of inbetween years
                while (added_year > firstClicked.year) {
                    selected[added_year] = {};
                    for (var i = 0; i < 12; i++) {
                        selected[added_year][i] = [];
                        for (var d = 1; d <= getDaysInMonth(added_year, i); d++) {
                            selected[added_year][i].push(d);
                        }
                    }
                    added_year = added_year - 1;
                }
            }

            if (added_month > firstClicked.month) {
                if (firstClicked.year == secondClicked.year) {
                    selected[added_year][added_month] = [];
                    for (var i = 1;
                    i <= secondClicked.date;
                    i++) {
                        selected[added_year][added_month].push(i);
                    }
                    added_month = added_month - 1;
                }
                while (added_month > firstClicked.month) {
                    selected[added_year][added_month] = [];
                    for (var i = 1;
                    i <= getDaysInMonth(added_year, added_month);
                    i++) {
                        selected[added_year][added_month].push(i);
                    }
                    added_month = added_month - 1;
                }
                added_date = getDaysInMonth(added_year, added_month);
            }

            for (var i = firstClicked.date + 1;
            i <= added_date;
            i++) {
                selected[added_year][added_month].push(i);
            }
        }
        return selected;
    }
});