
function sendit(){
	var frm = document.JoinusForm;
	
	if(frm.ID.value == "") {
		alert("이메일을 입력해주세요")
		frm.ID.focus();
		return false;
	}
	
	var exptext = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
    if(exptext.test(frm.ID.value) == false){
        alert("이메일 형식이 맞지 않습니다.!");
        frm.ID.focus();
        return false;
    }
	
	if(frm.Fname.value == "") {
		alert("귀하의 성을 입력해주세요")
		frm.Fname.focus();
		return false;
	}
	if(frm.Lname.value == "") {
		alert("귀하의 이름을 입력해주세요")
		frm.Lname.focus();
		return false;
	}
	if(frm.Password.value == "") {
		alert("비밀번호을 입력해주세요")
		frm.Password.focus();
		return false;
	}
	if(frm.Password.value.length < 4 || frm.Password.value.length > 16){
        alert("패스워드는 4자이상 16자 이하로 입력 해주세요!");
        frm.Password.focus();
        return false;
    }
	frm.submit();
}