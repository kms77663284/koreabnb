<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.dto.*" %>
<%@ page import="java.io.File" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
	List<Hotel> list = new ArrayList<Hotel>();
	if(request.getAttribute("HotelList")!=null){
		list = (List<Hotel>)request.getAttribute("HotelList");
	}
	
	
%>

    
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     


<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">
        
        	
				<form action="HotelAddAction" method="post" name="HostForm">
                
                    <div class="form-group ">
                    	<p class="p_start ">어떻게 시작 하고싶으세요?</p>
                    	<div class="div-mar-top-bot">
                        <div class="custom-control custom-radio offset-1 ">
                            <input type="radio" id="customRadio0" name="selectH_idx" value="0" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio0"><img class="hMainImg" src="<%=request.getContextPath() %>/images/q.gif"  id="qimg"><span class="textsize5 ">새로운 숙소 등록하기</span></label>
                        </div>
                        </div>
                        <hr>
                        <p class="p_edit set">숙소 등록중</p>
                        <%if(list.size()==0){%><h4 class="text-center">등록중인 숙소가 없습니다. 새로운 숙소 등록을 해주세요.</h4><%} %>
                        <%int i =1;
                    	for(Hotel hotel : list ){
                    		String imgPath="";
                    		String dirPath =request.getRealPath(File.separator)+ "/images/hotel/"+hotel.getH_idx();
                    		File file = new File(dirPath);
                    		File fList[] = file.listFiles();
                    		if(!file.isDirectory()||fList.length ==0){
                    			imgPath=request.getContextPath()+"/images/q.gif";%>
                    			<div class="custom-control custom-radio offset-1">
			                          <input type="radio" id="customRadio<%=i %>" name="selectH_idx" value="<%=hotel.getH_idx() %>" class="custom-control-input">
			                          <label  class="custom-control-label" for="customRadio<%=i%>"><img class="hMainImg" src="<%=imgPath%>"><span class="textsize5"><%if(hotel.getH_title()!=null){%><%=hotel.getH_title()%><%} %></span></label>
			                     	 </div>
			                     	 <hr>
					                      
                    			<%
                    		}else{
                    			if(fList.length>0){
                    				imgPath = request.getContextPath()+"/images/hotel/"+hotel.getH_idx()+"/"+fList[0].getName();%>
									<div class="custom-control custom-radio offset-1">
			                          <input type="radio" id="customRadio<%=i %>" name="selectH_idx" value="<%=hotel.getH_idx() %>" class="custom-control-input">
			                          <label class="custom-control-label" for="customRadio<%=i%>"><img class="hMainImg" src="<%=imgPath%>"><span class="textsize5"><%if(hotel.getH_title()!=null){%><%=hotel.getH_title()%><%} %></span></label>
			                     	 </div>
			                     	 <hr>
			                     	<% 
			                     }
                    		}
                    		
                    		
                        i++;} %> 	
                        
                        
                    </div>
                </form>
		</div>
		<jsp:include page="HostFooter.jsp" />
</div>

</body>
</html>

