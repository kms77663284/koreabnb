<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.dao.HoteltypeDAO" %>
    <%@ page import="com.dto.*" %>
<%@ page import="com.dto.User" %>
<%
	
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
//새로운 숙소 등록 했을때 랑 변경 해서 다시왔을때
	request.setCharacterEncoding("UTF-8");
	String h_idx =request.getParameter("h_idx");
	
	
	User u =null;
	if(session.getAttribute("user")!=null){
		u=(User)session.getAttribute("user");
	}
	
	HoteltypeDAO dao = HoteltypeDAO.getInstance();
	
	ArrayList<String> type = new ArrayList<String>();
	ArrayList<String> type2 = new ArrayList<String>();
	type= dao.GetType1List();
	type2 = dao.GetType2List();

%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" > 

<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>
<%
	if(hotel.getH_type()==null&&hotel.getH_type2()==null){hotel.setH_type2("");hotel.setH_type("");}
if(request.getParameter("h_idx")==null){
	int idx =Integer.parseInt(request.getAttribute("h_idx").toString());
	hotel.setH_idx(idx);
}
%>
<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">
		
                <div class="row">
                <div class="col-md-12 ">
                  <p class="p_start ">1단계 : 기본사항을 입력하세요.</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 10%"></div>
                </div>
                </div>
                <br>
                <div class="col-md-6 offset-1 div-margin-top">
                
                <form action="HostLocation.jsp" method="post" name="HostForm">
                <p class="p_title"><%=u.getU_last_name() %>님 안녕하세요!</p>
                    <p class="p_title">숙소 등록을 시작해볼까요?</p><br>
                    <p class="p_content">등록하려는 숙소의 유형을 선택해주세요</p>
                	 
                	 
                    <div class="form-group">
                        <select class="custom-select select_size" name="h_type">
                        	<% for(String t1 : type){ %>
                            <option <%if(hotel.getH_type().equals(t1)||hotel.getH_idx()==0){ %>selected<%}else{} %> value="<%=t1%>"><%=t1%></option>
                            <%} %>
                        </select>
                      
                        
                        <select class="custom-select select_size" name="h_guests">
                                <option <%if(hotel.getH_guests()== 1 || hotel.getH_idx()==0){ %>selected <%} %> value="1">최대1명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 2){ %>selected <%} %> value="2">최대2명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 3){ %>selected <%} %> value="3">최대3명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 4){ %>selected <%} %> value="4">최대4명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 5){ %>selected <%} %> value="5">최대5명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 6){ %>selected <%} %> value="6">최대6명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 7){ %>selected <%} %> value="7">최대7명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 8){ %>selected <%} %> value="8">최대8명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 9){ %>selected <%} %> value="9">최대9명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 10){ %>selected <%} %> value="10">최대10명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 11){ %>selected <%} %> value="11">최대11명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 12){ %>selected <%} %> value="12">최대12명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 13){ %>selected <%} %> value="13">최대13명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 14){ %>selected <%} %> value="14">최대14명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 15){ %>selected <%} %> value="15">최대15명숙박가능</option>
                                <option <%if(hotel.getH_guests()== 16){ %>selected <%} %> value="16">최대16명숙박가능</option>
                          
                            </select>

                            
                    </div>

                    <br>
                    
                        <p class="p_title">등록하실 숙소의 종류는 무엇인가요?</p>
                         <select class="custom-select select_size" name="h_type2">
                        	<%for(String t2 : type2){ %>
                            <option <%if(hotel.getH_type2().equals(t2) ||hotel.getH_idx()==0){ %>selected <%} %> value="<%=t2%>"><%=t2%></option>
                           	<%} %>
                        </select>

        			<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">	
                	<input type="hidden" name="h_address1" value="<%=hotel.getH_address1()%>">
                	<input type="hidden" name="h_address2" value="<%=hotel.getH_address2()%>">
                	<input type="hidden" name="h_address3" value="<%=hotel.getH_address3()%>">
                	<input type="hidden" name="h_address4" value="<%=hotel.getH_address4()%>">
                	<input type="hidden" name="h_zipcode" value="<%=hotel.getH_zipcode()%>">
                	<input type="hidden" name="h_facilities" value="<%=hotel.getH_facilities()%>">

				</form>
				
                </div>
                </div>
                
            </div>
			<jsp:include page="HostFooter.jsp" />

</div>
        
        



</body>
</html>



























        

       




