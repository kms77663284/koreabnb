<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.Hotel" %>
<%@ page import ="java.io.File" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	  href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<title>KoreaBnB - Search</title>
<link href="css/loginbar.css" rel="stylesheet">
<link href="css/search.css" rel="stylesheet" >
<link href="<%=request.getContextPath()%>/css/paging.css" rel="stylesheet">
<%
	ArrayList<Hotel> tmp = new ArrayList<Hotel>();
	ArrayList<Hotel> list = (ArrayList<Hotel>)request.getAttribute("list");
	list = list==null?tmp:list;
	int cnt = list.size();
	String search = request.getParameter("search");
	String area = search;
	int zoom = 11;
	if (search == null || search.equals("")){
		area = "충북 괴산군 칠성면 쌍곡리";
		zoom = 13;
	}
	if (list.size() > 0 && list.get(0).getH_title().indexOf(search) != -1){
		area = list.get(0).getH_address1();
	}
	int hotelnum = (int)request.getAttribute("hotelnum");
	int cut = 5;
	int pagenum = hotelnum / cut + (hotelnum%cut>0?1:0);
	String curpage = request.getParameter("page");
	int cp = (curpage==null||curpage.equals(""))?1:Integer.parseInt(curpage);
%>
</head>
<body>
	<jsp:include page="Header.jsp"/>
	<div id="left">
		<p><%=hotelnum%>개의 숙소</p>
		<%for (int i=0; i < cnt; i++){%>
			<a href="DetailInfo?h_idx=<%=list.get(i).getH_idx()%>">
				<div class="Room" id="Room_<%=i%>">
					<div class="Room_img">
					<%File f = new File(request.getRealPath(File.separator)+"/images/hotel/"+list.get(i).getH_idx()+"/");
					String imgpath=request.getContextPath()+"/images/q.gif";
					if(!f.isDirectory()){System.out.print("폴더없음");%><img src="<%=imgpath%>">
						<%}else{ 
							File[] flist = f.listFiles();
							if(flist.length>0){imgpath =  request.getContextPath()+"/images/hotel/"+list.get(i).getH_idx()+"/"+flist[0].getName();}%>
						<img src="<%=imgpath%>"><%} %>
					</div>
					<div class="Room_info">
		 				<div class="Room_header">
							<div class="Room_address"><%=list.get(i).getH_address1()%> <%=list.get(i).getH_address2() %></div>
							<div class="Room_star">★ <%=list.get(i).getH_star() %></div>
						</div>
						<div class="Room_body">
							<p><%=list.get(i).getH_type2()%> <%=list.get(i).getH_type() %></p>
							<p><%=list.get(i).getH_title() %></p>
						</div>
						<div class="Room_footer">
							<p>인원 <%=list.get(i).getH_guests() %>명</p>
							<p><%=list.get(i).getH_facilities() %></p>
						</div>
					</div>
				</div>
			</a>
		<%}%>
		<ul class="pagination">
			<li class="page-item"><a class="page-link" onclick="paging('b')">&laquo;</a></li>
		<%for (int i=cp>3?cp-2:1; i <= ((pagenum>=cp+2)?cp+2:pagenum); i++){%>
			<li class="page-item <%if(i==cp){%>active<%}%>"><a class="page-link" onclick="paging(<%=i%>)"><%=i%> </a></li>
		<%}%>
			<li class="page-item"><a class="page-link" onclick="paging('f')">&raquo;</a></li>
		</ul>
	</div>
	<div id="map"></div>
</body>
	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=1745d308df291261c0f18ff1268d942e&libraries=services,clusterer"></script>
    <script>
    	$(function(){
    		var mapContainer = document.getElementById('map'),
    	    mapOption = {
    	        center: new kakao.maps.LatLng(33.450701, 126.570667),
    	        level: <%=zoom%>
    	    };    
    		var map = new kakao.maps.Map(mapContainer, mapOption); 
    		var geocoder = new kakao.maps.services.Geocoder();
    		geocoder.addressSearch('<%=area%>', function(result, status) {
	    	     if (status === kakao.maps.services.Status.OK) {
	    	        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);
	    	        map.setCenter(coords);
	    	    	} 
    		});
    		<%for (int i=0; i < cnt; i++){%>
				var imageSrc = 'images/marker_<%=i+1%>.png',
		    		imageSize = new kakao.maps.Size(34, 40),
		    		imageOption = {offset: new kakao.maps.Point(27, 69)};
		
				var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
				    markerPosition = new kakao.maps.LatLng(<%=list.get(i).getH_mapY()%>,<%=list.get(i).getH_mapX()%>);
		
				var marker = new kakao.maps.Marker({
				  position: markerPosition,
				  image: markerImage
				});

				marker.setMap(map);  
    		<%}%>
    	});
    	
        $(window).scroll(function () {
            var elementPosition = $('#map').offset();
            if ($(window).scrollTop() + 130 > elementPosition.top) {
                $('#map')
                    .css('position', 'fixed')
                    .css('top', '120px')
                    .css('left', '48%');
            } else {
                $('#map')
                    .css('position', 'relative')
                    .css('left', '0%');
            }
        });
        
        window.onload = function () {
            setTimeout(function () {
                scrollTo(0, 0);
            }, 100);
        }
        paging = function(page){
        	if (page == 'b'){
        		var p = parseInt($('#h_page').val());
        		if (p > 1){
        			$('#h_page').val(p-1);
        		}
        	}
        	else if (page == 'f'){
        		var p = parseInt($('#h_page').val());
        		if (p < <%=pagenum%>){
        			$('#h_page').val(p+1);
        		}
        	}
        	else{
        		$('#h_page').val(page);
        	}
        	$('#headerForm').submit();
        }
	</script>
</html>