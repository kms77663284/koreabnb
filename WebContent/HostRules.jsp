<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dao.RuleDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");
    
    RuleDAO dao = RuleDAO.getInstance();
    List<String> list = new ArrayList<String>();
    
    list = dao.selectRules();
    %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>

<%String myRules[] = hotel.getH_rules().split(","); %>

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">

                <p class="p_start">3단계 : 게스트를 맞이할 준비를 하세요.</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 offset-1 div-margin-top">
                        <form action="HostPrice.jsp" method="post" name="HostForm">
                            <p class="p_title">게스트가 지켜야할 숙소 이용규칙을 정하세요</p>
                            <p class="p_content">게스트는 예약하기 전에 숙소 이용규칙에 동의해야합니다.</p>
                            <%int i=0;for(String rName : list){ %>
                            <div class="custom-control custom-switch">
                                    <input  type="checkbox" class="custom-control-input" id="customSwitch<%=i %>" name="h_rules" value="<%=list.get(i)%>"
                                    <%for(int j=0;j<myRules.length;j++){
                                    if(rName.equals(myRules[j])){%>checked<%}else{ %><%}%>
                                    <%} %>
                                    >
                                    <label class="custom-control-label label-mt20" for="customSwitch<%=i%>"><span class="label-mt20"><%=list.get(i) %></span></label>
                            </div><br><br>
							<%i++;} %>
							<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_price" value="<%=hotel.getH_price()%>">
                            	<input type="hidden" name="h_res_not_possible_date" value="<%=hotel.getH_res_not_possible_date()%>">

                        </form>
                    </div>

       </div>
			
		</div>

<jsp:include page="HostFooter.jsp" />
</div>
        
        



</body>
</html>























