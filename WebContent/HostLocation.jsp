<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");
    
    %>
    
<!DOCTYPE html>
<html>
<head>
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<script  src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<%=request.getContextPath() %>/js/host.js"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="//dapi.kakao.com/v2/maps/sdk.js?appkey=1745d308df291261c0f18ff1268d942e&libraries=services"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=1745d308df291261c0f18ff1268d942e&libraries=services"></script>

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
   
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">

			<div class="row">
				<div class="col-md-12 ">
					<p class="p_start offset-1">1단계 : 기본사항을 입력하세요.</p>
					<div class="progress">
						<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 15%"></div>
					</div>
				</div>
				
				<div class="col-md-5 offset-1 div-margin-top">
				<p class="p_title">숙소의 위치를 알려주세요</p>
                <p class="p_content">정확한 숙소 주소는 게스트가 예약을 완료한 후에만 공개합니다.</p>
                <form action="HostAmenities.jsp" method="post"  name="HostForm">
                    <button type="button" class="btn btn-outline-secondary" onclick="zip_address()">주소 검색</button>
                        
                        		<div class="row">
	                                <div class="col-md-4">
	                                <label for="sido">시/도</label>
	                                <input name="h_address1" type="text" class="form-control" id="h_address1" placeholder="예)서울특별시" value="<%if(hotel.getH_address1().equals("null")){}else{%><%=hotel.getH_address1() %><%} %>" required>
	                                
	                                </div>
	                            
	                                <div class="col-md-4">
	                                <label for="sigungu">시/군/구</label>
	                                <input name="h_address2" type="text" class="form-control" id="h_address2" placeholder="예)강남구" value="<%if(hotel.getH_address2().equals("null")){}else{%><%=hotel.getH_address2() %><%} %>" required>
	                                
	                                </div>
                                </div>
                            
                            <div class="mb-3">
                                    <label for="doro">도로명주소</label>
                                    <div class="input-group">
                                      <input name="h_address3" type="text" class="form-control" id="h_address3" placeholder="예)언주로길12" value="<%if(hotel.getH_address3().equals("null")){}else{%><%=hotel.getH_address3() %><%} %>" required>
                                      <div class="invalid-feedback" style="width: 100%;">
                                        도로명주소를 입력하세요.
                                      </div>
                                    </div>
                            </div>
                            <div class="mb-3">
                                    <label for="donghosu">동호수(선택사항)</label>
                                    <div class="input-group">
                                      <input name="h_address4" type="text" class="form-control" id="h_address4" placeholder="예)35동 4층 408호" value="<%if(hotel.getH_address4().equals("null")){}else{%><%=hotel.getH_address4() %><%} %>" required>
                                    </div>
                            </div>
                            <div class="col-md-6" style="padding:0;">
                                    <label for="donghosu">우편번호</label>
                                    <div class="input-group">
                                      <input name="h_zipcode" type="text" class="form-control" id="h_zipcode" placeholder="예)135-919" value="<%if(hotel.getH_zipcode().equals("null")){}else{%><%=hotel.getH_zipcode() %><%} %>" required>
                                    </div>
                            </div>
                        
                        
					<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
					<input type="hidden" name="h_type" value="<%=hotel.getH_type()%>">
					<input type="hidden" name="h_type2" value="<%=hotel.getH_type2()%>">
					<input type="hidden" name="h_guests" value="<%=hotel.getH_guests()%>">
					<input type="hidden" name="h_facilities" value="<%=hotel.getH_facilities()%>">
					<input type="hidden" name="h_mapX">
					<input type="hidden" name="h_mapY">
                    
                </form>
				</div>
				<div class="col-md-5 offset-1 div-margin-top">
					<div id="map" style="width:300px;height:300px;margin-top:10px;display:none"></div>
				</div>
			</div>
		</div>

	<footer class="HostFooter">
		<input class="btn btn-primary btn-lg  btn-mar-left btn-pad-top" role="button" type="button" onclick="HostPresclick()" value="이전">
			<input class="btn btn-primary btn-lg btn-fr btn-pad-top btn-mar-right" role="button" type="button" onclick="send()" value="다음">
	</footer>

</div>
        



</body>
</html>























