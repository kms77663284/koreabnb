<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.File" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");

	
	
    %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">
 			<div class="row">
                <div class="col-md-12 ">
                <p class="p_start">2단계 : 상세 정보를 제공해주세요.</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 43%"></div>
                </div>
                </div>
                <br>
              
                    <div class="col-md-6 offset-1">
                        <p class="p_title">숙소 사진 올리기</p>
                        <p class="p_content">게스트가 사진을 보고 숙소의 느낌을 생생히 떠올려볼 수 있도록 해주세요. 우선 사진 1장을 업로드하고 숙소를 등록한 후에 추가 할수있습니다. </p>
                        <form action="FileUpload?h_idx=<%=hotel.getH_idx() %>" enctype="multipart/form-data" name="HostForm" method="post">
                        <div class="form-group">
<%
String dirPath =request.getRealPath(File.separator)+ "/images/hotel/"+hotel.getH_idx();
String imgPath = "";
File dir = new File(dirPath);
try{
	
	
	if(!dir.exists()){

		System.out.println("디렉토리가 없습니다. 새로만듭니다."+ dir.getPath());
		
		dir.mkdirs();
	
		imgPath = request.getContextPath()+"/images/q.gif";
		for(int i=0;i<5;i++){
%>
								
                            <img src="<%=imgPath %>" id="foo<%=i+1 %>" class="img-thumbnail potoimg">
                             
                            <%if((i+1)%2==0){ %>
                             <br><br>
<%                          } 
                                
		}

		
	}else{
		System.out.println("디렉토리가 존재합니다."+ dir.getPath());

		File img = new File(dirPath);
		
		File[] fList = img.listFiles();
		
		for(int i=0;i<fList.length;i++){ //폴더안에 파일개수만큼돌면서
			
			
			if(fList[i].isFile()){
				imgPath=fList[i].getPath();
				
				
				imgPath = request.getContextPath()+"/images/hotel/"+hotel.getH_idx()+"/"+fList[i].getName();
				//System.out.println(imgPath);
				
				
%>

							
                                <img id="foo<%=i+1 %>" src="<%=imgPath %>" class="img-thumbnail potoimg">
                                <%if((i+1)%2==0){ %>
                                <br><br>
<%                                } 
                                
                                
                              


			}
		}
		
		for(int i = fList.length;i<5;i++){%>
					<img id="foo<%=i+1 %>" src="<%=request.getContextPath() %>/images/q.gif" class="img-thumbnail potoimg">
		<%}



	}

}catch(Exception e1){}

                                    
                                    
%>
</div>
                            <input name="file1" type="file" id="imgInp1" class="form-control " aria-describedby="fileHelp">
                            <input name="file2" type="file" id="imgInp2" class="form-control " aria-describedby="fileHelp">
                            <input name="file3" type="file" id="imgInp3" class="form-control " aria-describedby="fileHelp">
                            <input name="file4" type="file" id="imgInp4" class="form-control " aria-describedby="fileHelp">
                            <input name="file5" type="file" id="imgInp5" class="form-control " aria-describedby="fileHelp">
                                
								
                            	<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_title" value="<%=hotel.getH_title()%>">
                            	<input type="hidden" name="h_introduce" value="<%=hotel.getH_introduce()%>">
                            	<input type="hidden" name="h_other" value="<%=hotel.getH_other()%>">
                            	

                           

                        </form>
                    </div>
                    <div class="col-md-4">
                        <br><br><br><br>
                        <div class="card border-dark mb-3" style="max-width: 20rem;">
                            <div class="card-header">!!!</div>
                            <div class="card-body">
                              <h4 class="card-title">멋진 숙소 사진을 위한 팁</h4>
                              <p class="card-text">침실, 욕실, 거실 등 게스트가 사용할 공간을 모두 포함하세요. <br><br> 공간을 최대한 넓게 포착할 수 있도록 화면을 가로로 놓고 사진을 찍으세요
                                  코너에서 찍으면 전체를 사진에 더 잘 담을 수 있습니다.   <br>자연 채광으로 찍을때 가장 멋진 사진이 나와요. 밤이면 조명을 켜주세요. 플래시를 사용은 피하는게 좋습니다.
                                  <br><br>숙소를 등록한 후에 전문 사진 찰영 서비스를 요청할수도 있습니다.
                              </p>
                            </div>
                          </div>
                        
                    </div>
            </div>
        </div>


       </div>
               
         
		
<jsp:include page="HostFooter.jsp" />


</div>
        
        


<script type="text/javascript">
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#foo1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#foo2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#foo3').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#foo4').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL5(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#foo5').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp1").change(function() {
        readURL1(this);
    });
    $("#imgInp2").change(function() {
        readURL2(this);
    });
    $("#imgInp3").change(function() {
        readURL3(this);
    });
    $("#imgInp4").change(function() {
        readURL4(this);
    });
    $("#imgInp5").change(function() {
        readURL5(this);
    });
    
</script>
</body>
</html>




