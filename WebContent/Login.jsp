<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/login.css">
<script  src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<%=request.getContextPath() %>/js/login.js"></script>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
        }
</style>
</head>
<body>
	<div id="wrap">
            <div id="header">
	        	<jsp:include page="Header.jsp" />
	    	</div>
            <div class="login_box">
                <div class="login_box_container">
                    <form method="post" action="Login" name="LoginForm" id="LoginForm">
                        <p class="id_box">
                            <input type="text" name="ID" id="login_input_id" placeholder="이메일주소">
                            <i class="fa fa-envelope"></i>
                        </p>
                        <p class="pw_box"><input type="password" name="Password" id="login_input_pwd" placeholder="비밀번호">
                            <i class="fa fa-unlock-alt"></i>
                        </p>
                        <p><input type="button" onclick="inputcheck()" value="로그인" class="login_button"></p>
                    </form>
                    <div class="entry_box">
                        <p>에어비앤비 계정이 없으세요?</p>
                        <a href="Join.jsp">회원가입</a>
                    </div>
                </div>
            </div>
        </div>
</body>
	
</html>
