<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/joinus.css">
<script src="js/joinus.js"></script>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
        }
</style>
</head>
<body>
	<div id="wrap">
	    <div id="header">
	        <jsp:include page="Header.jsp" />
	    </div>
	    <div id="main">
			<div class="joinus_box">
                <div class="joinus_box_container">
                    <div class="p_box_1">
                        <a href="">구글</a>또는<a href="">카카오톡</a>으로 회원 가입하세요
                    </div>
                    <span></span>
                    <form action="Signin" method="post" name="JoinusForm">
                        <p><input type="text" name="ID" placeholder="이메일주소">
                            <i class="fa fa-envelope-o"></i>
                        </p>
                        <p><input type="text" name="Fname" placeholder="이름(예: 길동)">
                            <i class="fa fa-user-o"></i>
                        </p>
                        <p><input type="text" name="Lname" placeholder="성(예: 홍)">
                            <i class="fa fa-user-o"></i>
                        </p>
                        <p><input type="password" name="Password" placeholder="비밀번호 설정">
                            <i class="fa fa-eye-slash"></i>
                        </p>
                        <p><input type="button" value="가입하기" class="joinus_button" onclick="sendit()"></p>
                    </form>
                    <div class="login_box">
                        <p>이미 에어비앤비 계정이 있나요?</p>
                        <a href="Login.jsp">로그인</a>
                    </div>
                </div>
            </div>
        </div>
	</div>
</body>
</html>
