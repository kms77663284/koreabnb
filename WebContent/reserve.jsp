<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.Hotel" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.dto.User" %>
<%
	User user =null;
	
	if(session.getAttribute("user")!=null){
		user= (User)session.getAttribute("user");
	}
	if (user == null){response.sendRedirect("Login.jsp");}
%>
<%
	request.setCharacterEncoding("utf-8");
	Hotel hotel = (Hotel)request.getAttribute("hotel");
	hotel = hotel==null?new Hotel():hotel;
	String ppl = (String)request.getAttribute("ppl");
	String checkin =(String)request.getAttribute("checkin");
	String checkout =(String)request.getAttribute("checkout");
	String startYYYY = checkin.substring(0,4);
	String startMM = checkin.substring(5,7);
	String startDD = checkin.substring(8,10);
	String endYYYY=checkout.substring(0,4);
	String endMM=checkout.substring(5,7);
	String endDD=checkout.substring(8,10);
	
	
	long date=0;
	if(request.getAttribute("date")!=null){
		date=(long)request.getAttribute("date");
	}
	
	
%>
    
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css?family=Bad+Script&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&display=swap&subset=korean" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/reserve.css"> 



<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>
<form action="reserve.html" method="post">
<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
<div id="container">
            <div id="rule-box">
                <div id="title">
                    <h2>숙소 이용규칙 확인하기</h2>
                    <div id="opp">
                        <img src="images/reserve_icon.jpg">
                        <span>흔치 않은 기회입니다.</span>
                        이 숙소는 보통 예약이 가득 차 있습니다.
                    </div>
                </div>
                <div id="check-date">
                    <h3><%=hotel.getH_address1() %> <%=date %>박</h3>
                    <div></div>
                    <div class="content1">
                        <div class="content1_1"><%=startMM %>월 <%=startDD %>일
                        </div>
                        <div class="content1_2">체크인:<br>15:00</div>
                    </div>
                    <div class="content1">
                        <div class="content1_1"><%=endMM %>월 <%=endDD %>일</div>
                        <div class="content1_2">체크아웃: <br>11:00</div>
                    </div>
                </div>
                <div class="content2">
                    <h3>주의할 사항</h3>
                    <%
                    	String[] rules = hotel.getH_rules().split(",");
                    	for (String rule : rules){
                    %>
                    <div class="content2_1">
                        <div class="icon_note"><%=rule%> 가능</div>
                    </div>
                    <%}%>
                </div>
                <div id="button">
                	<form method="post" action="reserve2.jsp" name="sub">
                		<input type="hidden" value="<%=request.getParameter("checkin") %>" name="checkin">
                		<input type="hidden" value="<%=request.getParameter("checkout") %>" name="checkout">
                    	<input type="hidden" value="<%=hotel.getH_idx() %>" name="h_idx">
                    	<input type="hidden" value="<%=date %>" name="date">
                    	<input type="hidden" value="<%=ppl%>" name="ppl">
                    	
                    	<input type="submit" value="동의 후 계속하기">
                    </form>
                </div>
            </div>
            <div id="margin-box"></div>
            <div id="pay-box">
                <div id="popup">
                        <div id="popuptitle">
                            <h4><%=hotel.getH_title() %></h4>
                            <h5><%=hotel.getH_address1() %>의 <%=hotel.getH_type2() %>의 <%=hotel.getH_type() %><br><br></h5>
                            <img src="<%=request.getContextPath() %>/images/icon4_1.jpg" id="popup_img">
                            게스트 <%=ppl %>명<br><br>
                            <img src="<%=request.getContextPath() %>/images/icon4_2.jpg" id="popup_img">
                            <%=startYYYY %>년 <%=startMM %>월 <%=startDD %>일 -> <%=endYYYY %>년 <%=endMM %>월 <%=endDD %>일
                        </div>
                        <div class="content3">
                            <div class="content3_1 content3_2 content3_3">
                                ₩<%=hotel.getH_price() %> x <%=date%>박 x <%=ppl %>명
                                <span class="money">₩<%=hotel.getH_price() * date * Integer.parseInt(ppl)%></span></div>
                            <div class="content3_1 content3_3">
                                청소비
                                <span class="money">₩10,000</span></div>
                            <div class="content3_1 content3_3">
                                서비스 수수료
                                <span class="money">₩<%=Math.floor((hotel.getH_price() * date)*0.10)%></span></div>
                            <div class="content3_1 content3_3">
                                숙박세와 수수료<span class="money">₩<%=Math.floor((hotel.getH_price() * date)*0.01)%></span></div>
                        </div>
                        <div class="content3">
                            <div class="content3_1 content3_4">
                                총 합계 (KRW)
                                <span class="money">₩<%=Math.floor(10000+hotel.getH_price() * date * Integer.parseInt(ppl) +(hotel.getH_price() * date)*0.10+(hotel.getH_price() * date)*0.01) %></span></div>
                        </div>
                   
                </div>
            </div>
        </div>
              
		</div>


</div>
        
        


 </form>
</body>

</html>
