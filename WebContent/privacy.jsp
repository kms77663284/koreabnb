<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){
		user = new User();
		response.sendRedirect("Login.jsp");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Insert title here</title>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/mypage.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/privacy.css">

 <style>

        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }
        

    </style>



</head>
<body>
<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
<div class="container">
            <div class="container_box">
                <span class="title_box">
                    <a href="MyPage.jsp">계정 관리</a>
                    <span>&gt;</span>
                    <a href="#" class="privacy_a">개인 정보</a>
                </span>
                <h2>개인 정보</h2>
                <div class="content_box">
                    <div class="content2">
                        <section>
                            <div class="privacy_con">
                                <div class="modify_box">
                                    <h3>실명</h3>
                                    <button class="modify mo1" id="1">수정</button>
                                </div>
                                <p class="change1"><%=user.getU_first_name()%> <%=user.getU_last_name() %></p>
                            </div>
                            <div class="privacy_con">
                                <div class="modify_box">
                                    <h3>이메일 주소</h3>
                                </div>
                                <p class="change2"><%=user.getU_id() %></p>
                            </div>
                            <div class="privacy_con">
                                <div class="modify_box">
                                    <h3>전화번호</h3>
                                    <button class="modify mo3" id="2">수정</button>
                                </div>
                                <p class="change3">010-0000-0000</p>
                            </div>
                            <div class="privacy_con">
                                <div class="modify_box">
                                    <h3 >주소</h3>
                                    <button class="modify mo4" id="3">수정</button>
                                </div>
                                <p class="change4"><%=user.getU_residence() %></p>
                            </div>
                        </section>
                        <div class="content_r">
                            <svg
                                viewbox="0 0 24 24"
                                role="presentation"
                                aria-hidden="true"
                                focusable="false"
                                style="height:32px;width:32px;display:block;fill:#60B6B5">
                                <path
                                    d="m18.66 6.51-14.84 9.65a1 1 0 0 0 .55 1.84h15.62a1 1 0 0 0 1-1v-9.24a1.5 1.5 0 0 0 -2.32-1.26z"></path>
                                <path
                                    d="m9.25 12a1.25 1.25 0 1 1 -1.25-1.25 1.25 1.25 0 0 1 1.25 1.25zm11.75-8h-14a .5.5 0 0 0 0 1h14a1 1 0 0 1 1 1v12a1 1 0 0 1 -1 1h-1.5a.5.5 0 0 0 0 1h1.5a2 2 0 0 0 2-2v-12a2 2 0 0 0 -2-2zm-5 15h-13a1 1 0 0 1 -1-1v-12a1 1 0 0 1 1-1h1a .5.5 0 0 0 0-1h-1a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h13a .5.5 0 0 0 0-1zm-7.17-11.17a4.25 4.25 0 1 0 3.42 4.17 4.21 4.21 0 0 0 -.46-1.92.5.5 0 0 0 -.89.45 3.25 3.25 0 0 1 -.61 3.77 3.67 3.67 0 0 0 -4.56.02 3.25 3.25 0 0 1 2.27-5.57 3.3 3.3 0 0 1 .63.06.5.5 0 1 0 .19-.98zm5.67 3.17h5.5a.5.5 0 0 0 0-1h-5.5a.5.5 0 0 0 0 1zm0 2h5.5a.5.5 0 0 0 0-1h-5.5a.5.5 0 0 0 0 1zm0 2h5.5a.5.5 0 0 0 0-1h-5.5a.5.5 0 0 0 0 1z"
                                    fill="#484848"></path>
                            </svg>
                            <h3>다른 사람에게 어떤 정보가 공개되나요?</h3>
                            <p>에어비앤비는 예약이 확정된 후에만 호스트 및 게스트의 연락처 정보를 공개합니다.</p>
                        </div>
                    </div>
                </div>
            </div>
		</div>
</div>
</body>
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<script>
		$('button').click(function(){
			if ($(this).html() == '수정'){
				$(this).html('저장');
				var inputtag = '<input type="text" id="';
				inputtag = inputtag + $(this).attr('id') + 'in" value="' + $(this).parent().next().next().text() + '">';
				$(this).parent().after(inputtag);
				$(this).parent().next().next().css('display','none');
			}else{
				$(this).html('수정');
				var change = $('#'+$(this).attr('id')+'in').val();
				if (change != $(this).parent().next().next().text() && change != ""){
					var exptext = /^[0-9]{3}-[0-9]{3,4}-[0-9]{4}/;
					if ($(this).attr('id') == '2' && exptext.test(change) == false){
						alert("전화번호 형식이 맞지 않습니다.");
					} else if ($(this).attr('id') == '1' && change.split(' ').length != 2){
						alert("이름 성의 형식에 맞춰주세요.");
					}
					else{
						$.ajax({
							url: 'UpdatePrivacy',
							type: 'POST',
							data:{
								sel: $(this).attr('id'),
								val: change
							}
						});
						$(this).parent().next().next().text(change);
					}
				}
				$(this).parent().next().remove();
				$(this).parent().next().css('display','block');
			}
		})
	</script>
</html>