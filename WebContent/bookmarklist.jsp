<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.File" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
	List<Hotel> list = new ArrayList<Hotel>(); 
	if(request.getAttribute("hotelList")!=null){
		list = (List<Hotel>)request.getAttribute("hotelList");
	}
	
	


%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bookmark.css">

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
<div id="count-box">
            <div id="count">숙소 <%=list.size() %>개</div>
        </div>
        <div id="container">
        <form action="bookmark" method="post">
       <%
       		for(Hotel h : list){
       %>	
       <a href="DetailInfo?h_idx=<%=h.getH_idx()%>">
            <div class="Room">
                <div class="Room_img">
<%
					
					String dirPath =request.getRealPath(File.separator)+ "/images/hotel/"+h.getH_idx();
					File file = new File(dirPath);
					File fList[] = file.listFiles();
					String imgPath=request.getContextPath()+"/images/hotel/"+h.getH_idx()+"/"+fList[0].getName();
%>	
                    <img src="<%=imgPath%>">
                </div>
                <div class="Room_info">
                    <div class="Room_detail"><%=h.getH_type2() %></div>
                    <div class="Room_title"><%=h.getH_title() %></div>
                    <div class="Room_bottom">
                        <div class="Room_pay">
                            <span>￦<%=h.getH_price() %>/박</span><br>
                            <span><%=h.getH_star() %></span>
                        </div>
                        <input type="hidden" name="h_idx" value="<%=h.getH_idx()%>">
                        <input type="submit" value="삭제" class="del_btn">
                    </div>
                </div>
            </div>
            
            
            </a>
          <% } %>
            
           </form>
            </div>
        </div>
            
        


</div>

</body>
</html>







