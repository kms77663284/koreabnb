<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8"); 
	String now = (String) session.getAttribute("now");

	String dashboard = "dashboard";
	String notice = "notice";
	String question = "question";
	String aduser = "aduser";
	String reservation = "reservation";
	String room = "room";
	String property = "property";
	String review = "review";
	String amenities = "amenities";
	String api = "api";
	String joinUs = "joinUs";
	String siteSetting = "siteSetting";

	if(now == dashboard){
		dashboard = "class='active'";
	}else if (now == notice){
		notice = "class='active'";
	}else if(now == question){
		question = "class='active'";
	}else if(now == aduser){
		aduser = "class='active'";
	}else if(now == reservation){
		reservation = "class='active'";
	}else if(now == room){
		room = "class='active'";
	}else if(now == property){
		property = "class='active'";
	}else if(now == review){
		review = "class='active'";
	}else if(now == amenities){
		amenities = "class='active'";
	}else if (now == api){
		api = "class='active'";
	}else if(now == joinUs){
		joinUs = "class='active'";
	}else{
		siteSetting = "class='active'";
	}
%>

<div id="navbar" class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		<li <%=dashboard %>><a href="<%=request.getContextPath()%>/dashboard">대쉬보드</a> </li>
		<li <%=notice %>><a href="<%=request.getContextPath()%>/Admin/notice_sch.jsp">공지사항관리</a></li>
		<li <%=question %>><a href="<%=request.getContextPath()%>/Admin/question.jsp">문의사항</a></li>
	</ul>
	<ul class="nav nav-sidebar">
		<li <%=aduser %>><a href="<%=request.getContextPath()%>/Admin/user.jsp">유저관리</a></li>
		<li <%=reservation %>><a href="<%=request.getContextPath()%>/Admin/reservation.jsp">예약관리</a></li>
		<li <%=room %>><a href="<%=request.getContextPath()%>/adminroom">숙소관리</a></li>

		<li <%=property %>><a href="<%=request.getContextPath()%>/adminproperty">숙소종류/이용규칙설정</a></li>
		<li <%=review %>><a href="<%=request.getContextPath()%>/adminreview">리뷰관리</a></li>
		<li <%=amenities %>><a href="<%=request.getContextPath()%>/amenities">편의시설설정</a></li>
	</ul>
	<ul class="nav nav-sidebar">
		<li <%=api %>><a href="<%=request.getContextPath()%>/Admin/api.jsp">api</a></li>
		<li <%=joinUs %>><a href="<%=request.getContextPath()%>/Admin/joinUs.jsp">join us</a></li>
		<li <%=siteSetting %>><a href="<%=request.getContextPath()%>/Admin/siteSetting.jsp">사이트관리</a></li>
	</ul>
</div>