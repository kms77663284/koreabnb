<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "notice"); %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>notice_registration</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>
<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<%=request.getContextPath() %>/Admin/notice.jsp">공지사항 관리</a>
					</li>
					<li class="breadcrumb-item active">공지 등록</li>
				</ol>
				<form action="<%=request.getContextPath() %>/notice_regi" method="post">
					<div class="notnull">
						<p>
							제목 : <input type="text" name="n_title">
						</p>
						<p>내용 :</p>
						<textarea rows="20" name="n_content"></textarea>

					</div>
					<div class="sitesetting">
						<button type="submit" class="btn btn-primary">등록</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>