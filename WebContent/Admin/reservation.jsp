<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "reservation"); %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.Booking" %>
<%@ page import="com.dao.BookingDAO" %>
<%@ include file="paging.jsp" %>
<%
	BookingDAO dao = BookingDAO.getInstance();
	ArrayList<Booking> booking = dao.getBookingList(pageNum, search, catg);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>reservation</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>

<body>
	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">예약 관리</li>
				</ol>
				<div>
					<form action="<%=request.getContextPath() %>/Admin/reservation.jsp" method="post" class="form-inline my-2 my-lg-0 searching">
						<select name="catg" class="form-control" id="exampleSelect1">
							<option value="" ></option>
							<option value="b_idx">예약 번호</option>
							<option value="h_id">호스트 아이디</option>
							<option value="u_id">유저 아이디</option>
							<option value="b_date">예약 날짜</option>
							<option value="b_deposit">결 제(Y or N)</option>
							<option value="b_checkin">체크인</option>
							<option value="b_checkout">체크아웃</option>
						</select> <input name="search"class="form-control mr-sm-2" type="text"
							placeholder="Search">
						<button class="btn btn-secondary my-2 my-sm-0" type="submit">검색</button>
					</form>
				</div>
				<div class="table-responsive">
					<table class="table table-striped resad">
						<thead>
							<tr>
								<th class="first">예약 번호</th>
								<th class="second">호스트 아이디</th>
								<th class="third">유저 아이디</th>
								<th class="forth">예약 날짜</th>
								<th class="fifth cent">결제</th>
								<th class="sixth">체크인</th>
								<th class="seventh">체크아웃</th>
								<th class="eighth"></th>
								<th class="ninth"></th>
							</tr>
						</thead>
						<tbody>
						<%
						for(int i = 0; i<booking.size(); i++){
						%>
							<tr>
								<td><%=booking.get(i).getB_idx() %></td>
								<td><%=booking.get(i).getB_hid() %></td>
								<td><%=booking.get(i).getB_uid() %></td>
								<td><%=booking.get(i).getB_date() %></td>
								<td class="cent">
								<%
								if(booking.get(i).isB_deposit()){
								%>	
									Y
								<%
								}else{
								%>
									N
								<%
								}
								%>
								</td>
								<td><%=booking.get(i).getB_checkin()%></td>
								<td><%=booking.get(i).getB_checkout()%></td>
								<td><a class="btn btn-primary btn-sm"
									href="<%=request.getContextPath()%>/Admin/reservation_detail.jsp?b_idx=<%=booking.get(i).getB_idx()%>&u_id=<%=booking.get(i).getB_uid()%>&h_idx=<%=booking.get(i).getB_hidx()%>" role="button">자세히 보기</a></td>
								<td></td>
							</tr>
						<%
						}
						%>	
						</tbody>
					</table>
					<div class="paging_box">
						<div class="paging">
							<ul class="pagination">
								<%
									int startPage = (Integer.parseInt(pageNum) / 5) * 5 + 1;
									if (Integer.parseInt(pageNum) % 5 == 0)
										startPage -= 5;
									int targetPage = (dao.targetPage(pageNum, search, catg) - (Integer.parseInt(pageNum)-1)*10)/10 ; 
									if (startPage != 1) {
								%>
								<li class="page-item"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/reservation.jsp?pageNum=<%=Integer.parseInt(pageNum) - 1%>&search=<%=search%>&catg=<%=catg%>">&laquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&laquo;</a></li>
								<%
									}
									for (int i = startPage; i < Integer.parseInt(pageNum); i++) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/reservation.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
									}
								%>
								<li class="page-item active"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/reservation.jsp?pageNum=<%=pageNum%>&search=<%=search%>&catg=<%=catg%>"><%=pageNum%></a></li>
								<%
									for (int i = Integer.parseInt(pageNum) + 1; i <= targetPage + Integer.parseInt(pageNum); i++) {
										if (i < startPage + 5) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/reservation.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
										}
									}
									if (targetPage + Integer.parseInt(pageNum) > startPage + 4) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/reservation.jsp?pageNum=<%=startPage + 5%>&search=<%=search%>&catg=<%=catg%>">&raquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&raquo;</a></li>
								<%
									}
								%>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>