<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "aduser"); %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dao.UserDAO" %>
<%@ include file="paging.jsp" %>
<%
	UserDAO dao = UserDAO.getInstance();
	ArrayList<User> userList = dao.getUserList(pageNum, search, catg);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>room</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>
<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">유저 관리</li>
				</ol>
				<div>
					<form method="post" action="<%=request.getContextPath()%>/Admin/user.jsp" class="form-inline my-2 my-lg-0 searching">
						<select name="catg" class="form-control" id="exampleSelect1">
							<option value=""></option>
							<option value="id">아이디</option>
							<option value="name">회원 이름</option>
							<option value="date">회원가입 날짜</option>
							<option value="activate">활성화(Y or N)</option>
						</select> <input name="search"class="form-control mr-sm-2" type="text"
							placeholder="Search">
						<button class="btn btn-secondary my-2 my-sm-0" type="submit">검색</button>
					</form>
				</div>
				<div class="table-responsive s_table">
					<table class="table table-striped userad">
						<thead>
							<tr>
								<th class="first">프로필 사진</th>
								<th>아이디</th>
								<th class="second">회원 이름</th>
								<th class="third cent">활성화</th>
								<th class="forth">회원가입 날짜</th>
								<th class="fifth"></th>
							</tr>
						</thead>
						<tbody>
						<%
							for(int i=0; i<userList.size(); i++){
						%>
							<tr>
								<td><img src="<%=request.getContextPath()%>/<%=userList.get(i).getU_potoaddress()%>"></td>
								<td><%=userList.get(i).getU_id() %></td>
								<td><%=userList.get(i).getU_first_name()%> <%=userList.get(i).getU_last_name()%></td>
								<td class="cent">
								<%
									if(userList.get(i).isActivate()){
								%>
									Y
								<%
									}else{
								%>
									N
								<%
									}
								%>
								</td>
								<td><%=userList.get(i).getU_date()%></td>
								<td><a class="btn btn-primary btn-sm" href="<%=request.getContextPath()%>/user_del?u_id=<%=userList.get(i).getU_id()%>" role="button">삭제</a></td>
							</tr>
						<%
							}
						%>
						</tbody>
					</table>
					<div class="paging_box">
						<div class="paging">
							<ul class="pagination">
								<%
									int startPage = (Integer.parseInt(pageNum) / 5) * 5 + 1;
									if (Integer.parseInt(pageNum) % 5 == 0)
										startPage -= 5;
									int targetPage = (dao.targetPage(pageNum, search, catg) - (Integer.parseInt(pageNum)-1)*10)/10 ; 
									if (startPage != 1) {
								%>
								<li class="page-item"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/user.jsp?pageNum=<%=Integer.parseInt(pageNum) - 1%>&search=<%=search%>&catg=<%=catg%>">&laquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&laquo;</a></li>
								<%
									}
									for (int i = startPage; i < Integer.parseInt(pageNum); i++) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/user.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
									}
								%>
								<li class="page-item active"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/user.jsp?pageNum=<%=pageNum%>&search=<%=search%>&catg=<%=catg%>"><%=pageNum%></a></li>
								<%
									for (int i = Integer.parseInt(pageNum) + 1; i <= targetPage + Integer.parseInt(pageNum); i++) {
										if (i < startPage + 5) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/user.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
										}
									}
									if (targetPage + Integer.parseInt(pageNum) > startPage + 4) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/user.jsp?pageNum=<%=startPage + 5%>&search=<%=search%>&catg=<%=catg%>">&raquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&raquo;</a></li>
								<%
									}
								%>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>