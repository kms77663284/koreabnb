<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%	request.setCharacterEncoding("UTF-8");%>
<%	session.setAttribute("now", "siteSetting");%>
<%@ page import="com.dao.SiteSettingDAO" %>
<%@ page import="com.dto.SiteSetting" %>
<%
	SiteSettingDAO dao = SiteSettingDAO.getInstance();
	SiteSetting tmp =  new SiteSetting();
	SiteSetting site = dao.getSiteSetting(tmp);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>site setting</title>
<link href="<%=request.getContextPath()%>/css/admin.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/dashboard.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/paging.css"
	rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">

			<%@ include file="Nav_bar.jsp"%>


			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">site setting</li>
				</ol>
				<form action="#" method="post">
					<div class="apiJoinUs">
						<p>Version of Web-Site</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="1,000" value="<%=tmp.getS_ver()%>">
					</div>
					<div class="apiJoinUs">
						<p>Hello human!</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_intro()%>">
					</div>
					<div class="apiJoinUs">
						<p>Logo</p>
						<input type="file" class="form-control-file" id="exampleInputFile"
							aria-describedby="fileHelp" name="logo"> <small id="fileHelp"
							class="form-text text-muted">이미지 해상도 xxx이하</small>
					</div>
					<div class="apiJoinUs">
						<p>Minimun Price</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_min()%>">
					</div>
					<div class="apiJoinUs">
						<p>Maximun Price</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_max()%>">
					</div>
					<div class="apiJoinUs">
						<p>Defualt Language</p>
						<select class="form-control" id="exampleSelect1">
							<option value="" selected="">Korean</option>
						</select>
					</div>
					<div class="apiJoinUs">
						<p>Main Image</p>
						<input type="file" class="form-control-file" id="exampleInputFile"
							aria-describedby="fileHelp" name="main"> <small id="fileHelp"
							class="form-text text-muted">이미지 해상도 xxx이하</small>
					</div>

					<div class="sitesetting">
						<button type="button" class="btn btn-primary" onclick="not()">수정</button>
					</div>
				</form>

			</div>
		</div>
	</div>
<script>
	function not(){
		alert("수정할 수 있는 권한이 없습니다.");
	}
</script>

</body>
</html>