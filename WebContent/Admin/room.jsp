<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.Hotel" %>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "room"); %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>room</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>
<%
	ArrayList<Hotel> list = (ArrayList<Hotel>)request.getAttribute("list");
	int hotelnum = (int)request.getAttribute("hotelnum");
	int cut = 10;
	int pagenum = hotelnum / cut + (hotelnum%cut>0?1:0);
	String curpage = request.getParameter("page");
	int cp = (curpage==null||curpage.equals(""))?1:Integer.parseInt(curpage);
%>
<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">숙소 관리</li>
				</ol>
				<div>
					<form class="form-inline my-2 my-lg-0 searching" action="adminroom" method="get" id="r_search">
						<input type="hidden" name="page" id="c_page" value="<%=cp%>">
						<select name="catg" class="form-control" id="exampleSelect1" name="sel">
							<option value="null" selected></option>
							<option value="h_idx">숙소 번호</option>
							<option value="h_id">호스트 이메일</option>
							<option value="h_title">숙소 이름</option>
							<option value="h_type2">숙소 유형</option>
							<option value="h_type">숙소 종류</option>
							<option value="h_star">평 점</option>
							<option value="h_price">가 격</option>
							<option value="h_date">등록 날짜</option>
							<option value="h_grant">허 가</option>
						</select> <input name="search" class="form-control mr-sm-2" type="text" name="search"
							placeholder="Search">
						<button class="btn btn-secondary my-2 my-sm-0" type="submit">검색</button>
					</form>
				</div>
				<div class="table-responsive">
					<table class="table table-striped roomad">
						<thead>
							<tr>
								<th class="first">숙소 번호</th>
								<th class="second">호스트 이메일</th>
								<th class="forth">숙소 이름</th>
								<th class="fifth">숙소 유형</th>
								<th class="sixth">숙소 종류</th>
								<th class="seventh">평 점</th>
								<th class="eighth">가 격</th>
								<th class="ninth">등록 날짜</th>
								<th class="tenth">허 가</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<% for (Hotel tmp : list){ %>
							<tr>
								<td><%=tmp.getH_idx() %></td>
								<td><%=tmp.getH_id() %></td>
								<td><%=tmp.getH_title() %></td>
								<td><%=tmp.getH_type() %></td>
								<td><%=tmp.getH_type2() %></td>
								<td><%=tmp.getH_star() %></td>
								<td><%=tmp.getH_price() %></td>
								<td><%=tmp.getH_date() %></td>
								<td class="cent"><%=tmp.isH_grant()?"Y":"N" %></td>
								<td><%if (!tmp.isH_grant()) {%><a class="btn btn-primary btn-sm"
									role="button" onclick="grant(<%=tmp.getH_idx()%>)">승인</a><%}%> <a class="btn btn-primary btn-sm"
									role="button" onclick="del(<%=tmp.getH_idx()%>)">삭제</a></td>
							</tr>
						<%} %>
						</tbody>
					</table>
					<div class="paging_box">
						<div class="paging">
							<ul class="pagination">
								<li class="page-item"><a class="page-link" onclick="paging('b')">&laquo;</a></li>
							<%for (int i=cp>3?cp-2:1; i <= ((pagenum>=cp+2)?cp+2:pagenum); i++){%>
								<li class="page-item <%if(i==cp){%>active<%}%>"><a class="page-link" onclick="paging(<%=i%>)"><%=i%> </a></li>
							<%}%>
								<li class="page-item"><a class="page-link" onclick="paging('f')">&raquo;</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	paging = function(page){
	    if (page == 'b'){
	    	var p = parseInt($('#c_page').val());
	    	if (p > 1){
	    		$('#c_page').val(p-1);
	    	}
	    }
	    else if (page == 'f'){
	    	var p = parseInt($('#c_page').val());
	    	if (p < <%=pagenum%>){
	    		$('#c_page').val(p+1);
	    	}
	    }
	    else{
	    	$('#c_page').val(page);
	    }
	    $('#r_search').submit();
	}
	grant = function(id){
		$.ajax({
			url: 'adminroom',
			type: 'POST',
			data:{
				h_idx: id,
				met: 'permit'
			}
		});
		location.reload();
	}
	del = function(id){
		$.ajax({
			url: 'adminroom',
			type: 'POST',
			data:{
				h_idx: id,
				met: 'del'
			}
		});
		location.reload();
	}
</script>
</html>