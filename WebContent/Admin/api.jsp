<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "api"); %>
<%@ page import="com.dao.SiteSettingDAO" %>
<%@ page import="com.dto.SiteSetting" %>
<%
	SiteSettingDAO dao = SiteSettingDAO.getInstance();
	SiteSetting tmp = new SiteSetting();
	SiteSetting site = dao.getSiteSetting(tmp);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>api</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">api 관리</li>
				</ol>
				<form action="#" method="post">
					<div class="apiJoinUs">
						<p>Facebook Client ID</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_api1()%>">
					</div>
					<div class="apiJoinUs">
						<p>Facebook Secret Code</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_api2()%>">
					</div>
					<div class="apiJoinUs">
						<p>Goolgle Client ID</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_api3()%>">
					</div>
					<div class="apiJoinUs">
						<p>goolgle Secret Code</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_api4()%>">
					</div>
					<div class="apiJoinUs">
						<p>Goolgle Map Browser Key</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_api5()%>">
					</div>
					<div class="apiJoinUs">
						<p>Goolgle Map Server Key</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_api6()%>">
					</div>
					<div class="sitesetting">
						<button type="button" class="btn btn-primary" onclick="not()">수정</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<script>
	function not(){
		alert("수정할 수 있는 권한이 없습니다.");
	}
</script>
</body>
</html>