<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "joinUs"); %>
<%@ page import="com.dao.SiteSettingDAO" %>
<%@ page import="com.dto.SiteSetting" %>
<%
	SiteSettingDAO dao = SiteSettingDAO.getInstance();
	SiteSetting tmp = new SiteSetting();
	SiteSetting join = dao.getSiteSetting(tmp);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>join us</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">join us 관리</li>
				</ol>
				<form action="#" method="post">
					<div class="apiJoinUs">
						<p>Facebook</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_facebook()%>">
					</div>
					<div class="apiJoinUs">
						<p>Instagram</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_instagram()%>">
					</div>
					<div class="apiJoinUs">
						<p>App Store</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_googleStore()%>">
					</div>
					<div class="apiJoinUs">
						<p>Itunes Store</p>
						<input class="form-control form-control-lg apijoinus" type="text"
							value="<%=tmp.getS_itunesStore()%>">
					</div>
					<div class="sitesetting">
						<button type="submit" class="btn btn-primary">수정</button>
					</div>
				</form>

			</div>
		</div>
	</div>
<script>
	function not(){
		alert("수정할 수 있는 권한이 없습니다.");
	}
</script>
</body>
</html>