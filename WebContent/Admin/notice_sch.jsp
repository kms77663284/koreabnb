<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "notice"); %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.dto.Notice"%>
<%@ page import="com.dao.NoticeDAO"%>
<%@ include file="paging.jsp" %>
<%
	NoticeDAO ndo = NoticeDAO.getInstance();
	ArrayList<Notice> noticeList = ndo.searchNotice(pageNum, search, catg);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>notice</title>
<link href="<%=request.getContextPath()%>/css/admin.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/dashboard.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/paging.css"
	rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>


			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">공지사항 관리</li>
				</ol>
				<div>
					<form action="<%=request.getContextPath() %>/Admin/notice_sch.jsp" method="post"
						class="form-inline my-2 my-lg-0 searching">
						<select name="catg" class="form-control" id="exampleSelect1">
							<option value=""></option>
							<option value="n_idx">공지번호</option>
							<option value="n_title">공지제목</option>
							<option value="a_id">작성자</option>
							<option value="n_date">날짜</option>
						</select> <input name="search" class="form-control mr-sm-2" type="text"
							placeholder="Search">
						<button class="btn btn-secondary my-2 my-sm-0" type="submit">검색</button>
					</form>
				</div>
				<div class="table-responsive">
					<table class="table table-striped noti">
						<thead>
							<tr>
								<th class="first">공지번호</th>
								<th class="second">공지제목</th>
								<th class="third">조회수</th>
								<th class="forth">작성자</th>
								<th class="fifth">공지사항 등록날짜</th>
								<th class="sixth"></th>
							</tr>
						</thead>
						<tbody>

							<%
								for (int i = 0; i < noticeList.size(); i++) {
							%>
							<tr>
								<td><%=noticeList.get(i).getN_idx()%></td>
								<td><a
									href="<%=request.getContextPath()%>/notice_view?n_idx=<%=noticeList.get(i).getN_idx()%>">
										<%=noticeList.get(i).getN_title()%></a></td>
								<td><%=noticeList.get(i).getN_vcount()%></td>
								<td><%=noticeList.get(i).getA_id()%></td>
								<td><%=noticeList.get(i).getN_date()%></td>
								<td><a class="btn btn-primary btn-sm"
									href="<%=request.getContextPath()%>/Admin/notice_mod.jsp?n_idx=<%=noticeList.get(i).getN_idx()%>"
									role="button">수정</a> / <a class="btn btn-primary btn-sm"
									href="<%=request.getContextPath()%>/notice_del?n_idx=<%=noticeList.get(i).getN_idx()%>"
									role="button">삭제</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					<div class="paging_box">
						<div class="paging">
							<ul class="pagination">
								<%
									int startPage = (Integer.parseInt(pageNum) / 5) * 5 + 1;
									if (Integer.parseInt(pageNum) % 5 == 0)
										startPage -= 5;
									int targetPage = (ndo.targetPage_search(pageNum, search, catg) - (Integer.parseInt(pageNum)-1)*10)/10 ;
									if (startPage != 1) {
								%>
								<li class="page-item"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/notice_sch.jsp?pageNum=<%=Integer.parseInt(pageNum) - 1%>&search=<%=search%>&catg=<%=catg%>">&laquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&laquo;</a></li>
								<%
									}
									for (int i = startPage; i < Integer.parseInt(pageNum); i++) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/notice_sch.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
									}
								%>
								<li class="page-item active"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/notice_sch.jsp?pageNum=<%=pageNum%>&search=<%=search%>&catg=<%=catg%>"><%=pageNum%></a></li>
								<%
									for (int i = Integer.parseInt(pageNum) + 1; i <= targetPage + Integer.parseInt(pageNum); i++) {
										if (i < startPage + 5) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/notice_sch.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
										}
									}
									if (targetPage + Integer.parseInt(pageNum) > startPage + 4) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/notice_sch.jsp?pageNum=<%=startPage + 5%>&search=<%=search%>&catg=<%=catg%>">&raquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&raquo;</a></li>
								<%
									}
								%>
							</ul>
						</div>
						<div class="regi">
							<a class="btn btn-primary btn-lg"
								href="<%=request.getContextPath()%>/Admin/notice_regi.jsp"
								role="button"> 등 록 </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>