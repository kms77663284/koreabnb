<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "reservation"); %>
<%@ page import="com.dto.Booking" %>
<%@ page import="com.dto.User" %>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dao.BookingDAO" %>
<%@ page import="com.dao.HotelDAO" %>
<%@ page import="com.dao.UserDAO" %>
<%@ include file="paging.jsp" %>
<%
	String u_id = request.getParameter("u_id");
	int h_idx = Integer.parseInt(request.getParameter("h_idx"));	
	int b_idx = Integer.parseInt(request.getParameter("b_idx"));

	System.out.print(u_id);
	System.out.print(h_idx);
	System.out.print(b_idx);
	UserDAO udao = UserDAO.getInstance();
	HotelDAO hdao = HotelDAO.getInstance();
	BookingDAO bdao = BookingDAO.getInstance();
	
	User user1 = udao.getUser(u_id);
	Hotel hotel = hdao.getHotel(h_idx);
	Booking booking = bdao.getBooking(b_idx);

	
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>reservation</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main re_detail">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="reservation.jsp">예약
							관리</a></li>
					<li class="breadcrumb-item"><a href="reservation.jsp">자세히
							보기</a></li>
					<li class="breadcrumb-item active"><%=booking.getB_idx()%></li>
				</ol>
				<div class="table-responsive">
					<h3>예약 정보</h3>
					<table class="table table-striped resad_detail">
						<thead>
							<tr>
								<th class="first">예약번호</th>
								<th class="second">예약 시간</th>
								<th class="second">체크인</th>
								<th class="second">체크아웃</th>
								<th class="cent">결제</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><%= booking.getB_idx() %></td>
								<td><%= booking.getB_date() %></td>
								<td><%= booking.getB_checkin() %></td>
								<td><%= booking.getB_checkout() %></td>
								<td class="cent">
								<%
								if(booking.isB_deposit()) {
								%>
									Y
								<%
								}else{
								%>
									N
								<%
								}
								%>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<h3>숙소 정보</h3>
					<table class="table table-striped roomad">
						<thead>
							<tr>
								<th class="first">숙소 번호</th>
								<th class="second">숙소 이름</th>
								<th class="third">호스트 아이디</th>
								<th class="forth">숙소 소개</th>
								<th class="fifth">숙소 유형</th>
								<th class="sixth">숙소 종류</th>
								<th class="seventh">평 점</th>
								<th class="eighth">가 격</th>
								<th class="ninth">등록 날짜</th>
								<th class="cent">허 가</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><%=hotel.getH_idx() %></td>
								<td><%=hotel.getH_title() %></td>
								<td><%=hotel.getH_id() %></td>
								<td><%=hotel.getH_introduce() %></td>
								<td><%=hotel.getH_type2() %></td>
								<td><%=hotel.getH_type() %></td>
								<td><%=hotel.getH_star() %></td>
								<td><%=hotel.getH_price() %></td>
								<td><%=hotel.getH_date() %></td>
								<td class="cent">
								<%
								if(hotel.isH_grant()){
								%>
									Y
								<%
								}else {
								%>
									N
								<%
								}
								%>
								
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<h3>회원 정보</h3>
					<table class="table table-striped userad">
						<thead>
							<tr>
								<th class="first">프로필 사진</th>
								<th>아이디</th>
								<th class="second">회원 이름</th>
								<th class="third cent">활성화</th>
								<th class="forth">회원가입 날짜</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><img src="<%=request.getContextPath()%>/<%=user1.getU_potoaddress()%>"></td>
								<td><%=user1.getU_id() %></td>
								<td><%=user1.getU_first_name()%> <%=user1.getU_last_name()%></td>
								<td class="cent">
								<%
									if(user1.isActivate()){
								%>
									Y
								<%
									}else{
								%>
									N
								<%
									}
								%>
								</td>
								<td><%=user1.getU_date() %></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="regi">
					<a class="btn btn-primary btn-lg" href="<%=request.getContextPath()%>/Admin/reservation.jsp"
						role="button"> 이 전 </a>
				</div>
			</div>
		</div>
	</div>

</body>
</html>