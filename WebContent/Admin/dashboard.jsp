<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "dashboard"); %>
<%@ page import="com.dto.Count" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Dashboard</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<%
Count tmp = new Count();
Count count = (Count)request.getAttribute("count");
count = count==null?tmp:count;
%>
</head>

<body>
	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">Home</li>
				</ol>

				<div class="row placeholders">
					<div class="col-xs-6 col-sm-4 placeholder">
						<div class="small-box bg-yellow">
							<div class="inner">
								<h3><%=count.getU_count() %></h3>
								<p>Total Users</p>
							</div>
							<a href="user.jsp" class="small-box-footer">"More Info" <i
								class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 placeholder">
						<div class="small-box bg-green">
							<div class="inner">
								<h3><%=count.getH_count() %></h3>
								<p>Total Rooms</p>
							</div>
							<a href="room.jsp" class="small-box-footer">"More Info" <i
								class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 placeholder">
						<div class="small-box bg-blue">
							<div class="inner">
								<h3><%=count.getB_count() %></h3>
								<p>Total Reservations</p>
							</div>
							<a href="reservation.jsp" class="small-box-footer">"More
								Info" <i class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 placeholder">
						<div class="small-box bg-purple">
							<div class="inner">
								<h3><%=count.getU_tcount() %></h3>
								<p>Today Users</p>
							</div>
							<a href="user.jsp" class="small-box-footer">"More Info" <i
								class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 placeholder">
						<div class="small-box bg-pink">
							<div class="inner">
								<h3><%=count.getH_tcount() %></h3>
								<p>Today Rooms</p>
							</div>
							<a href="room.jsp" class="small-box-footer">"More Info" <i
								class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 placeholder">
						<div class="small-box bg-orange">
							<div class="inner">
								<h3><%=count.getB_tcount() %></h3>
								<p>Today Reservations</p>
							</div>
							<a href="reservation.jsp" class="small-box-footer">"More
								Info" <i class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>