<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "question"); %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.Question" %>
<%@ page import="com.dao.QuestionAdDAO" %>
<%@ include file="paging.jsp" %>
<%
	QuestionAdDAO dao = QuestionAdDAO.getInstance();
	ArrayList<Question> questionList = dao.getQuestion(pageNum, search, catg);
			
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>question</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>

<body>

	<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">문의사항 관리</li>
				</ol>
				<div>
					<form action="<%=request.getContextPath() %>/Admin/question.jsp" class="form-inline my-2 my-lg-0 searching">
						<select name="catg" class="form-control" id="exampleSelect1">
							<option value="" ></option>
							<option value="q_idx">문의 번호</option>
							<option value="u_id">유저 아이디</option>
							<option value="q_title">제목</option>
							<option value="q_date">작성시간</option>
							<option value="q_ans">답변 여부(Y or N)</option>
						</select> <input name="search"class="form-control mr-sm-2" type="text"
							placeholder="Search">
						<button class="btn btn-secondary my-2 my-sm-0" type="submit">검색</button>
					</form>
				</div>
				<div class="table-responsive">
					<table class="table table-striped question">
						<thead>
							<tr>
								<th class="first">문의 번호</th>
								<th class="second">유저 아이디</th>
								<th>제목</th>
								<th class="fifth">작성시간</th>
								<th class="third cent">답변여부</th>
								<th class="sixth"></th>
							</tr>
						</thead>
						<tbody>
						<%
							for(int i = 0; i<questionList.size(); i++){
						%>
							<tr>
								<td><%=questionList.get(i).getQ_idx() %></td>
								<td><%=questionList.get(i).getU_id() %></td>
								<td><a href="<%=request.getContextPath() %>/Admin/question_view.jsp?q_idx=<%=questionList.get(i).getQ_idx() %>"><%=questionList.get(i).getQ_title() %></a></td>
								<td><%=questionList.get(i).getQ_date() %></td>
								<td class="cent">
								<%
									if(questionList.get(i).getQ_answer() != null){
								%>
										Y
								<%
									}else {
								%>
										N
								<%
									}
								%>
								</td>
								<td><a class="btn btn-primary btn-sm"
									href="<%=request.getContextPath() %>/Admin/question_view.jsp?q_idx=<%=questionList.get(i).getQ_idx() %>" role="button">답변</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					<div class="paging_box">
						<div class="paging">
							<ul class="pagination">
								<%
									int startPage = (Integer.parseInt(pageNum) / 5) * 5 + 1;
									if (Integer.parseInt(pageNum) % 5 == 0)
										startPage -= 5;
									int targetPage = (dao.targetPage(pageNum, search, catg) - (Integer.parseInt(pageNum)-1)*10)/10 ;  ;
									if (startPage != 1) {
								%>
								<li class="page-item"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/question.jsp?pageNum=<%=Integer.parseInt(pageNum) - 1%>&search=<%=search%>&catg=<%=catg%>">&laquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&laquo;</a></li>
								<%
									}
									for (int i = startPage; i < Integer.parseInt(pageNum); i++) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/question.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
									}
								%>
								<li class="page-item active"><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/question.jsp?pageNum=<%=pageNum%>&search=<%=search%>&catg=<%=catg%>"><%=pageNum%></a></li>
								<%
									for (int i = Integer.parseInt(pageNum) + 1; i <= targetPage + Integer.parseInt(pageNum); i++) {
										if (i < startPage + 5) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/question.jsp?pageNum=<%=i%>&search=<%=search%>&catg=<%=catg%>"><%=i%></a></li>
								<%
										}
									}
									if (targetPage + Integer.parseInt(pageNum) > startPage + 4) {
								%>
								<li class="page-item "><a class="page-link"
									href="<%=request.getContextPath()%>/Admin/question.jsp?pageNum=<%=startPage + 5%>&search=<%=search%>&catg=<%=catg%>">&raquo;</a></li>
								<%
									} else {
								%>
								<li class="page-item disabled"><a class="page-link"
									href="#">&raquo;</a></li>
								<%
									}
								%>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>