<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<% 	session.setAttribute("now", "question"); %>
<%@ page import="com.dto.Question" %>
<%@ page import="com.dao.QuestionAdDAO" %>
<%
	int q_idx = Integer.parseInt(request.getParameter("q_idx"));

	QuestionAdDAO dao = QuestionAdDAO.getInstance();
	Question questionView = dao.viewQuestion(q_idx);
	
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>question</title>
<link href="<%=request.getContextPath() %>/css/admin.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/dashboard.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/paging.css" rel="stylesheet">
</head>

<body>

<%@ include file="Session.jsp"%>
	<div class="container-fluid">
		<div class="row">
		
			<%@ include file="Nav_bar.jsp" %>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item ">문의사항 관리</li>
					<li class="breadcrumb-item active">답 변</li>
				</ol>
				<form method="post" action="<%=request.getContextPath()%>/question_ans">
				<div class="notnull">
					<div class="ques_box">
						<p>문의 번호 : <%=questionView.getQ_idx() %></p>
						<p>문의 제목 : <%=questionView.getQ_title() %></p>
						<p>문의자 아이디 : <%=questionView.getU_id() %></p>
						<p>작성시간 : <%=questionView.getQ_date() %></p>
						<br>
						<p>내용 :</p>
						<p><%=questionView.getQ_content() %></p>

					</div>
					<p>답변 :</p>		
					<textarea rows="20" name="q_answer" placeholder="<%=questionView.getQ_answer()==null?"":questionView.getQ_answer() %>"></textarea>
					<input type="hidden" name="q_idx" value="<%=questionView.getQ_idx()%>">
				</div>
				<div class="sitesetting">
					<button type="submit" class="btn btn-primary">답변</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>