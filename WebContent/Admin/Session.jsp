<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% 	request.setCharacterEncoding("UTF-8"); %>
<%@ page import="com.dto.User" %>
<%	
	User user = (User)session.getAttribute("user");
	if (user == null || !user.isAdmin()){
		response.sendRedirect("../MainPage.jsp");
	}
	String id = "";
	id = user == null ? "" : user.getU_id();
%>	
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid container-fluid-color">
		<div class="navbar-header">
			<a class="navbar-brand" href="<%=request.getContextPath() %>/MainPage.jsp">Korea bnb</a>
		</div>
		<div class="navbar-collapse collapse">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<p id="dateDemo"></p>
			<script>
				var dateDemo = new Date();
				document.getElementById("dateDemo").innerHTML = dateDemo
						.getDate()
						+ "<span>/</span>"
						+ dateDemo.getDay()
						+ "<span" +
                                ">/</span>"
						+ dateDemo.getFullYear()
						+ " "
						+ dateDemo.getHours()
						+ "<span>:</sp" +
                            "an>"
						+ dateDemo.getMinutes()
						+ "<span>:</span>"
						+ dateDemo.getMilliseconds();
			</script>
			<ul class="nav navbar-nav navbar-right">
				<li><img
					src="<%=request.getContextPath()%>/images/avatar04.png"
					alt="admin face image" class="face-img">
					<p class="face-p"><%=id%></p></li>
				<li><a href="<%=request.getContextPath()%>/logout">logout</a></li>
			</ul>
		</div>
	</div>
</nav>
	
