<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.User" %>
<%@ page import="com.dto.Review" %>
<%@ page import="com.dao.ReviewDAO" %>
<%
	User user = (User)session.getAttribute("user");
	if(user == null) {
		user = new User();
		response.sendRedirect("Login.jsp");
	}
	int h_idx = Integer.parseInt(request.getParameter("h_idx"));
	ReviewDAO dao = ReviewDAO.getInstance();
	Review review =null;
	if(dao.checkReview(h_idx, user.getU_id())){review= dao.selectReview(h_idx, user.getU_id());}
	
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>KoreaBnb</title>
        <link rel="stylesheet" href="css/mypage.css">
        <link rel="stylesheet" href="css/mypage_inqury.css">
	</head>
	<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }
    </style>
	<body>
		<div id="wrap">
			<div id="header">
				<jsp:include page="Header.jsp" />
			</div>
			<div id="main">
		        <div class="container">
		            <div class="container_box">
		                <span class="title_box">
		                    <a href="MainPain.jsp">계정 관리</a>
		                    <span>&gt;</span>
		                    <a href="mypage_res?page=1">예약 내역</a>
		                    <span>&gt;</span>
		                   	후기 작성
		                </span>
		                <h2>후기 작성</h2>
		                <div class="content_box1">
		                    <form method="post" action="UploadReview">
		                    	<input type="hidden" name="h_idx" value="<%=h_idx%>">
		                    	<input type="hidden" name="star" value="0" id="r_star">
		                        <p>작성자 : <%=user.getU_first_name() %> <%=user.getU_last_name() %></p>
		                        <p id="star_grade">별점:<a href="#" id="1">★</a><a href="#" id="2">★</a><a href="#" id="3">★</a><a href="#" id="4">★</a><a href="#" id="5">★</a></p>
		                        <p>내용</p><textarea name="content" id="content" cols="150" rows="5"><%if(review!=null){%><%=review.getR_content()%><%}else{ %><%} %></textarea>
		                        <p><input type="submit" value="작성"></p>
		                    </form>
		                </div>
		          </div>
	          </div>
	    </div>
	</body>
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<script>
        $('#star_grade a').click(function(){
            $(this).parent().children("a").removeClass("on");  /* 별점의 on 클래스 전부 제거 */ 
            $(this).addClass("on").prevAll("a").addClass("on"); /* 클릭한 별과, 그 앞 까지 별점에 on 클래스 추가 */
            $('#r_star').val($(this).attr('id'));
            return false;
        });
	</script>
</html>