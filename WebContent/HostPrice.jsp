<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");
    String r[] = {};
    if(request.getParameterValues("h_rules")!=null){
    	r=request.getParameterValues("h_rules");
    }
    String rules="";
    for(int i =0;i<r.length;i++){
    	if(i< r.length-1){
    		rules+=r[i]+",";
    		
    	}else{
    		rules+=r[i];
    	}
    }
    
    %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     
<jsp:useBean id="hotel" class="com.dto.Hotel"></jsp:useBean>
<jsp:setProperty property="*" name="hotel"/>

<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">

                <p class="p_start">3단계 : 게스트를 맞이할 준비를 하세요</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 85%"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 offset-1 div-margin-top">
                        <form action="HostCalendar.jsp" method="post" name="HostForm">
                            <p class="p_title">숙소 요금 설정하기</p>
                            <p class="p_content">예약을 받을 가능성을 높이세요</p>
                            
                            <p class="p_title">기본요금</p>
                            
                            <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">￦</span>
                            </div>
                                <input name="h_price" value="<%=hotel.getH_price() %>" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                            </div>
                            	<input type="hidden" name="h_idx" value="<%=hotel.getH_idx()%>">
                            	<input type="hidden" name="h_res_not_possible_date" value="<%=hotel.getH_res_not_possible_date()%>">
                            	<input type="hidden" name="h_rules" value="<%=rules%>">
                            	

                       

                        </form>
                    </div>

       </div>
        </div>
			
		<jsp:include page="HostFooter.jsp" />
		


</div>
        
        



</body>
</html>























