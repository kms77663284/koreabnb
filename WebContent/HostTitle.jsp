<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
    request.setCharacterEncoding("utf-8");

   	String h_title = (String) request.getAttribute("h_title");
   	int h_idx = Integer.parseInt((String) request.getAttribute("h_idx").toString());
   	String h_other = (String) request.getAttribute("h_other");
   	String h_introduce = (String) request.getAttribute("h_introduce");

    %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostcss.css" >     


<title>KoreaBnb</title>
<style>
        #wrap {
            width: 100%; 
             
        }
        #main {
            padding-top:80px;
            width: 100%;
            z-index: 2;
  
        }

    </style>
</head>
<body>

<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main" class="up">

                <p class="p_start">2단계 : 상세 정보를 제공해주세요.</p>
                <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 65%"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-5 offset-1 div-margin-top">
                        <p class="p_title">이름 지정</p>
                        <p class="p_content">숙소의 특징과 장점을 강조하는 제목을 정해 게스트의 관심을 끌어보세요.</p>
                        <form action="HostDescription.jsp" method="post" name="HostForm">
                                    
                            <div class="form-group">
                                <input name="h_title" class="form-control form-control-lg" type="text" value="<%if(h_title.equals("null")){}else{%><%=h_title %><% }%>"  placeholder="이름 지정" id="inputLarge">
                            </div>

								<input type="hidden" name="h_idx" value="<%=h_idx%>">
                            	<input type="hidden" name="h_introduce" value="<%=h_introduce%>">
                            	<input type="hidden" name="h_other" value="<%=h_other%>">
                            	
                            	
                           

                        </form>
                    </div>
                    <div class="col-md-5 offset-1 div-margin-top">
                    	<img class="titleImg" src="<%=request.getContextPath() %>/images/hosttitle.png">
                    </div>
            </div>

		</div>

<jsp:include page="HostFooter.jsp" />
</div>
        
        



</body>
</html>























