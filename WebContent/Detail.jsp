<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dao.BookmarkDAO" %>
<%@ page import="com.dto.Hotel" %>
<%@ page import="com.dto.User" %>
<%@ page import="com.dto.Review" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.File" %>
<%
	request.setCharacterEncoding("utf-8");
	Boolean check=false;
	Hotel h=new Hotel();
	String[] fac={};
	String[] rul={};
 	if(request.getAttribute("hotel")!=null){
 		h=(Hotel)request.getAttribute("hotel");
 		if(h.getH_facilities()!=null)
 		{
 			fac = (h.getH_facilities()).split(",");
 		
 		}
 		if(h.getH_rules()!=null){
 			rul = (h.getH_rules()).split(",");
 		}
 	}

	ArrayList<Review> re = new ArrayList<Review>();
	if(request.getAttribute("review")!=null){
		re = (ArrayList<Review>)request.getAttribute("review");
	}
	User user = null;
	if(session.getAttribute("user")!=null){
		user = (User)session.getAttribute("user");
	}
	User h_user_info = new User();
	if(request.getAttribute("h_user_info") != null){
		h_user_info = (User)request.getAttribute("h_user_info");
	}
	
	
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>detail</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath() %>/css/detail.css">
        <link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap-datepicker.css">
		<script  src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/bootstrap-datepicker.js"></script>
		<script src="<%=request.getContextPath() %>/js/bootstrap-datepicker.ko.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/detail.js"></script>
        <style>
            #wrap {
                width: 100%;

            }
            #main {
                padding-top: 80px;
                width: 100%;
                z-index: 2;
            }
        </style>
     </head>
     <body>
     <div id="wrap">
         <div id="header">
             <jsp:include page="Header.jsp"/>
         </div>
         <div id="main">
	         <div class="detail_img">
<% 
		String imgPath=request.getContextPath()+"/images/q.gif";
		File dir = new File(request.getRealPath(File.separator)+"/images/hotel/"+h.getH_idx()+"/");
		ArrayList<String> imgPaths=new ArrayList<String>();
		for(int i=0;i<5;i++){imgPaths.add(imgPath);}
		if(!dir.isDirectory()){System.out.print("폴더없음");}
		else{
			File[] fList = dir.listFiles();
			if(fList.length>0){
				for(int i=0;i<fList.length;i++){
					imgPath= request.getContextPath()+"/images/hotel/"+h.getH_idx()+"/"+fList[i].getName();
					imgPaths.add(i, imgPath);
				}
			}
		}

%>
				<img src="<%=imgPaths.get(0)%>" id="detail_img1">
		         <div class="detail_img_box">
			         <img src="<%=imgPaths.get(1) %>" id="detail_img2">
			         <img src="<%=imgPaths.get(2) %>" id="detail_img3">
		         </div>
		         <div class="detail_img_box">
			         <img src="<%=imgPaths.get(3) %>" id="detail_img4">
			         <img src="<%=imgPaths.get(4) %>" id="detail_img5">
		         </div>
		         <form method="post" action="bookmark" name="bookmark1" id="detailPage_save" onclick="bookmark2('<%=user%>')">
		         	<input type="hidden" name="h_idx" value="<%=h.getH_idx()%>">
	                	<div id="saveImg_box"  >
<%
							BookmarkDAO dao = BookmarkDAO.getInstance();
							if(user!=null){ check = dao.bookmarkCehck(h.getH_idx(),user.getU_id());}
							
%>
	                    	<img src="images/heart<%if(check){%>2<%}else{%>1<%} %>.png" alt="저장아이콘이미지" id="hart_fill" >
	                    	
	                	</div> 저장
                	
            	 </form>
		     </div>
	         <div id="detail">
		         <div class="content">
		             <div id="summary"><%=h.getH_title() %></div>
		             <div id="location"><%=h.getH_address1() %></div>
		             <div id="content1">
		             	<img src="images/icon1_1.jpg"><%=h.getH_type2() %>의 <%=h.getH_type() %><br>
		                <span>인원 <%=h.getH_guests() %>명</span>
		             </div>
		             <div id="content2">
		             	<img src="images/icon1_2.jpg">높은 청결도<br>
		             	<span>최근 게스트 <%=re.size() %>명이 이 숙소가 티 없이 깨끗하다고 후기를 남겼습니다.</span>
		             </div>
		             <div id="content3">
		             	<img src="images/icon1_3.jpg"> 순조로운 체크인 과정<br>
		             	<span>별점 <%=h.getH_star() %>점을 준 숙소입니다.</span>
		         	 </div>
		         	<br>
		         	<div id="content4"><%=h.getH_introduce() %></div>
		         	<div id="content5">
		         		<div id="subsummary">편의시설</div>
		            	<div id="content5_1">
		            	<%for(int i =0;i<fac.length;i++){ %>
		            		<img src=""><%=fac[i] %><span id="content5_2"></span>
		            		<%} %>
		                	<img src="">
		            	</div>

		         	</div>
		         </div>
		         <div id="content7">
			         <div id="subsummary">예약 가능 여부</div>
			                   정확한 요금과 예약 가능 여부를 확인하려면 여행 일자를 입력하세요<br>
			         <div class="calendar calendar-first" id="calendar_first">
    					<div class="calendar_header">
        					<button class="switch-month switch-left"> 
        						<i class="fa fa-chevron-left"></i>
        					</button>
         					<h2></h2>
        					<button class="switch-month switch-right"> 
        						<i class="fa fa-chevron-right"></i>
        					</button>
    					</div>
    					<div class="calendar_weekdays"></div>
    					<div class="calendar_content"></div>
    					<input type="hidden" value="<%=h.getH_res_not_possible_date()%>" id="res_not">
					 </div>
		         </div>
		         <div id="content8">
		          	<div id="mid_summary">후기<br>★<%=h.getH_star() %> | <%=re.size() %>후기</div>
		         </div>
		         <div id="content9">
		         	<%if(re.size() == 0){ %>
		         		<p>후기가 없습니다.</p>
		         	<%}else{%>
			          	<%for(Review r : re){ %>
		                <div id="review">
		                	<img src="images/review2.jpg" id="review_img">
		               	</div>
		                <div id="review_summary"><%=r.getR_uid()%><br><%=r.getR_date() %></div>
		                <div id="review_content">
			               <%=r.getR_content() %>    <%if(user!=null){if(r.getR_uid().equals(user.getU_id())){%><input type="button" value="삭제" onclick="reviewdelete(<%=r.getR_idx()%>,<%=h.getH_idx()%>)"><%}else{ %><%}} %>     
		                </div>
		                
	                <%}} %>
	              </div>
	              <div id="content10">
	              	<div id="mid_summary">호스트: <%=h_user_info.getU_first_name() %>님<br>
	                	<div id="mid_summary1">
	                	<%=h_user_info.getU_id() %>,  회원 가입: <%=h_user_info.getU_residence() %>
	                   	</div>
	                </div>
	                <div id="review_content">
			            <%=h_user_info.getU_introduce() %>
		                <br>
						언어: English, 日本語<br>
		                              응답률: 100%<br>
		                              응답에 소요된 시간: 1시간 이내<br>
	                </div>
	                <div id="content11">
	                	<div id="mid_summary">지역정보</div>
	                    <div id="map" style="width:300px;height:200px;"></div>
	                </div>
					<div id="content12">
	                	<div id="mid_summary">유의 사항</div>
		                              체크인: 16:00 이후<br>
		                              체크아웃: 11:00<br>
		            </div>
	                <div id="content13">
	                	<div id="mid_summary">숙소이용규칙</div>
	                	<%for(int i=0;i<rul.length;i++){ %>
	                    <img src="" id="content13_1"> <%=rul[i] %> 가능
	                    <%} %>
	                     	
	                </div>
	                <div id="content14">
	                	<div id="mid_summary">예약 취소<br></div>엄격 정책
	                    <ul>
	                    	<li>체크인 30일 전까지 예약을 취소하면 모든 수수료를 포함한 요금 전액이 환불됩니다.</li>
	                        <br>
	                        <li>체크인까지 30일이 남지 않은 시점에 예약을 취소하면 수수료를 포함한 총 숙박 요금의 50%가 환불됩니다.</li>
	                        <br>
	                        <li>숙박 중 예약을 취소하면 일부 환불을 요청할 수 있습니다.</li>
	                        <br>
	                    </ul>
	                </div>
	             </div>
	
	             <!--예약하기 팝업창-->
	             <div id="popup">
	             	<form action="reserve_start" method="post" name="reservefrm">
	                	<div id="popuptitle">
	                    	<h4>요금을 확인하려면 날짜를 확인하세요</h4>
	                        <h5>날짜를 추가하여 정확한 요금을 확인하세요</h5>
							<div id="check">
	                                             날짜<br>
	                        	<input  name="checkin" id="checkin" placeholder="체크인" type="text" class="form-control input-group date">
	                       	</div>
	                        <p></p>
	                        <input name="checkout" id="checkout" placeholder="체크아웃" type="text" class="form-control input-group date">
	                    </div>
	                    <p></p>
						<div id="ppl">
	                                      인원<br>
	                    	<select id="ppl" name="ppl">
								<option value="1">1명</option>
								<option value="2">2명</option>
								<option value="3">3명</option>
								<option value="4">4명</option>
								<option value="5">5명</option>
								<option value="6">6명</option>
								<option value="7">7명</option>
								<option value="8">8명</option>
								<option value="9">9명</option>
								<option value="10">10명</option>
								<option value="11">11명</option>
								<option value="12">12명</option>
								<option value="13">13명</option>
								<option value="14">14명</option>
								<option value="15">15+명</option>
	                        </select>
	                        <p></p>
	                        <input type="hidden" name="h_idx" value="<%=h.getH_idx()%>">
	                        <input id="reservebtn" type="button" value="예약하기" onclick="reserveSend(<%=h.getH_guests()%>,'<%=h.getH_res_not_possible_date()%>')">
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=1745d308df291261c0f18ff1268d942e&libraries=services,clusterer"></script>
	<script>
	$(function(){
		var mapContainer = document.getElementById('map'),
	  	mapOption = {
	        center: new kakao.maps.LatLng(<%=h.getH_mapY()%>,<%=h.getH_mapX()%>),
	        level: 5
	    };    
		var map = new kakao.maps.Map(mapContainer, mapOption); 
		var imageSrc = 'images/marker_1.png',
	    imageSize = new kakao.maps.Size(34, 40),
	    imageOption = {offset: new kakao.maps.Point(27, 69)};

		var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
			markerPosition = new kakao.maps.LatLng(<%=h.getH_mapY()%>,<%=h.getH_mapX()%>);

		var marker = new kakao.maps.Marker({
			  position: markerPosition,
			  image: markerImage
			});

		marker.setMap(map);  
	})
	
	</script>
</body>
</html>