<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Bad+Script&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&display=swap&subset=korean" rel="stylesheet">
<title>KoreaBnB</title>
<link href="css/airbnb.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-latest.js"></script>
</head>
<body>
	<div class="container">
		<div class="bg_img">
			<img src="images/main.jpg" alt="main-image" width="100%" height="760px" id="bg_img_z">

			<header>
				<h1 id="logo">
					<a href="<%=request.getContextPath() %>/MainPage.jsp"><img src="images/logo.png"></a>
				</h1>
				<ul>
					<%if (user == null){ %>
					<li><a class="menuLink" href="Join.jsp">회원가입</a></li>
					<li><a class="menuLink" href="Login.jsp">로그인</a></li>
					<%}else{%>
					<%if(!user.isAdmin()){ %>
					<li><a class="menuLink" href="HotelMyList">숙소 호스팅</a></li>
					<li><a class="menuLink" href="MyPage.jsp">마이페이지</a></li>
					<%}else{ %>
					<li><a class="menuLink" href="dashboard">관리자  페이지</a></li>  
					<%} %>
					<li><a class="menuLink" href="Logout">로그아웃</a></li>
					<%} %>
				</ul>
			</header>
      
			<div id="main">
				<form  method="post" action="Search" name="searchForm">
					<div id="mainchoice">
						<h3>특색 있는 숙소와<br>즐길 거리를 예약하세요</h3>
						<div id="location" style="margin-top: -10px;">목적지<br><input type="text" id="location" name="search" placeholder="지역을 선택하세요 "></div>
						<p></p>
						<div id="checkin">
							 체크인<br><input type="text" id="checkin" name="dateStart" placeholder="년/월/일">
						</div>
						<p></p>
						<div id="checkout">
							체크아웃<br><input type="text" id="checkout" name="dateEnd" placeholder="년/월/일">
						</div>
						<p></p>
						<div id="ppl">
							인원<br>
							<select id="ppl" name="geusts" aria-placeholder="인원">
								<option value="1">1명</option>
								<option value="2">2명</option>
								<option value="3">3명</option>
								<option value="4">4명</option>
								<option value="5">5명</option>
								<option value="6">6명</option>
								<option value="7">7명</option>
								<option value="8">8명</option>
								<option value="9">9명</option>
								<option value="10">10명</option>
								<option value="11">11명</option>
								<option value="12">12명</option>
								<option value="13">13명</option>
								<option value="14">14명</option>
								<option value="15">15+명</option>
							</select>
							<p></p>
							<input type="submit" value="검색">
							<input type="hidden" name="page" value="1">
							<input type="hidden" name="type1">
							<input type="hidden" name="price">
						</div>
					</div>
				</form>           
			</div>
		</div>
        
		<div id="content1" class="content">
			<h2>추천 여행지</h2>
			<div id="recom1" class="recom">
				<h4><a onclick="recommend('서울시')">SEOUL</a></h4>
				<img src="images/seoul.jpg" alt="recom1">
			</div>
			<div id="recom2" class="recom">
				<h4><a onclick="recommend('제주도')">JEJU</a></h4>
				<img src="images/jeju.jpg" alt="recom2">
			</div>
			<div id="recom3" class="recom">
				<h4><a onclick="recommend('부산시')">BUSAN</a></h4>
				<img src="images/busan.jpg" alt="recom3">
			</div>
			<div id="recom4" class="recom">
				<h4><a onclick="recommend('광주시')">GWANG-JU</a></h4>
				<img src="images/gwangju.JPG" alt="recom4">
			</div>
			<div id="recom5" class="recom">
				<h4><a onclick="recommend('강원도')">GWANG-WON</a></h4>
				<img src="images/gwangwon.jpg" alt="recom5">
			</div>
		</div>
		<div id="content2" class="content best_container"></div>
<footer>
                <div id="footerimg">
               						                   
                    <div id="footercontent">
                        <ul>
                            <div id="footercontent1">
<h4>코리아비앤비</h4></div>
                            <div id="footercontent2">
					
                            <li><a class="menuLink" href="HotelMyList">숙소 호스팅</a></li><br>
                            <li><a class="menuLink" href="HostMyHotel">내 숙소</a></li><br>
                            <li><a class="menuLink" href="HostMyBooking">예약내역</a></li><br>
                            <li><a class="menuLink" href="BookmarkInfo">저장목록</a></li><br>
                        </div>
                        <div id="footercontent3">
                            <li><a class="menuLink" href="host/HostStart.html">도움말</h4></a></li><br>
                            <li><a class="menuLink" href="host/HostStart.html">이용약관</a></li><br>
                            <li><a class="menuLink" href="host/HostStart.html">공지사항</a></li><br>
                            <li><a class="menuLink" href="host/HostStart.html">문의</a></li><br>
                        </div>
                        <div id="footercontent4">
                            <li><a class="menuLink" href="Join.jsp">회원가입</a></li><br>
                            <li><a class="menuLink" href="Login.jsp">로그인</a></li><br>
                            <li><a class="menuLink" href="dashboard">관리자 페이지</a></li><br>  
                        </ul>
                    </div>
                    </div>
                    <div id="icon">
                        <a href="https://www.instagram.com/airbnb/"><img src="images/icon1.png"></a>
                        <a href="https://www.facebook.com/airbnb"><img src="images/icon2.png"></a>
                        <a href="https://twitter.com/airbnb"><img src="images/icon3.png"></a>
                    </div>
                
            </footer>
	</div>
</body>
<script>
	recommend = function(val){
		var frm = document.searchForm;
		frm.search.value = val;
		frm.submit();
	}
	window.onload =function(){
		$.ajax({
		    url : "favorite.jsp",
		    dataType : "html",
		    type : "post",
		    success : function(result){
		        $("#content2").html(result);
		    }
		});
	};
</script>
</html>