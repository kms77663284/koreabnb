<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.dto.Booking" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dto.User" %>
<%
	User user = (User)session.getAttribute("user");
	if (user == null){response.sendRedirect("Login.jsp");}
%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>KoreaBnB</title>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/hostbootstrap.min.css" >
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/mypage.css" >
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/mypage_res.css" >
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/paging.css" >
	 <style>
	        #wrap {
	            width: 100%; 
	        }
	        #main {
	            padding-top:80px;
	            width: 100%;
	            z-index: 2;
	        }
	</style>
	<%
		ArrayList<Booking> list = (ArrayList<Booking>)request.getAttribute("list");
		String curpage = request.getParameter("page");
		int bookingnum = (int)request.getAttribute("bookingnum");
		int cut = 5;
		int pagenum = bookingnum / cut + (bookingnum%cut>0?1:0);
		int cp = (curpage==null||curpage.equals(""))?1:Integer.parseInt(curpage);
	%>
	</head>
	<body>
	<div id="wrap">
        <div id="header">
            <jsp:include page="Header.jsp" />
        </div>
        <div id="main">
			<div class="container">
            	<div class="container_box">
                	<span class="title_box">
                    	<a href="MyPage.jsp">계정 관리</a>
                    	<span>&gt;</span>
                    	<a href="mypage_res?page=1">예약 내역</a>
                	</span>
                	<h2>예약 내역</h2>
                	<div class="content_box">
                    	<table>
                        	<tr>
	                            <th>호텔 이름</th>
	                            <th>호스트 아이디</th>
	                            <th>숙박 날짜</th>
	                            <th>숙박 인원</th>
	                            <th>주소</th>
	                            <th>총 가격</th>
	                            <th></th>
                        	</tr>
                        <%for (Booking tmp : list){%>
	                        <tr>
	                        	<td><a href="mypage_res_inqury?id=<%=tmp.getB_hidx()%>"><%=tmp.getB_htitle() %></a></td>
	                        	<td><%=tmp.getB_hid() %>
	                        	<td><%=tmp.getB_checkin() %> ~ <%=tmp.getB_checkout() %></td>
	                        	<td><%=tmp.getB_guests() %></td>
	                        	<td><%=tmp.getB_address() %></td>
	                        	<td><%=tmp.getB_price() %></td>
	                        	<td><button onclick="goreview(<%=tmp.getB_hidx()%>)">후기</button></td>
	                        </tr>
                        <%}%>
                    	</table>
	                    <ul class="pagination">
							<li class="page-item"><a class="page-link" onclick="paging('b')">&laquo;</a></li>
						<%for (int i=cp>3?cp-2:1; i <= ((pagenum>=cp+2)?cp+2:pagenum); i++){%>
							<li class="page-item <%if(i==cp){%>active<%}%>"><a class="page-link" onclick="paging(<%=i%>)"><%=i%> </a></li>
						<%}%>
							<li class="page-item"><a class="page-link" onclick="paging('f')">&raquo;</a></li>
						</ul>
                	</div>
            	</div>
        	</div>
		</div>
	</div>
	</body>
	<script>
	    paging = function(page){
	    	var p = <%=cp%>
	    	if (page == 'b'){
	    		if (p > 1) p = p-1;
	    	}
	    	else if (page == 'f'){
	    		if (p < <%=pagenum%>) p = p+1;
	    	}
	    	else{
	    		p = page;
	    	}
	    	location.href="mypage_res?page="+p;
	    }
	    
	    goreview = function(h_idx){
	    	location.href="review.jsp?h_idx="+h_idx;
	    }
	</script>
</html>