package com.dto;

public class Review {
	private int r_idx;
	private String r_uid;
	private int r_hidx;
	private String r_content;
	private String r_date;
	private double r_star;
	public int getR_idx() {
		return r_idx;
	}
	public void setR_idx(int r_idx) {
		this.r_idx = r_idx;
	}
	public String getR_uid() {
		return r_uid;
	}
	public void setR_uid(String r_uid) {
		this.r_uid = r_uid;
	}
	public int getR_hidx() {
		return r_hidx;
	}
	public void setR_hidx(int r_hidx) {
		this.r_hidx = r_hidx;
	}
	public String getR_content() {
		return r_content;
	}
	public void setR_content(String r_content) {
		this.r_content = r_content;
	}
	public String getR_date() {
		return r_date;
	}
	public void setR_date(String r_date) {
		this.r_date = r_date;
	}
	public double getR_star() {
		return r_star;
	}
	public void setR_star(double r_star) {
		this.r_star = r_star;
	}
}
