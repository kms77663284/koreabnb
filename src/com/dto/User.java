package com.dto;

public class User {
	private String u_id;
	private String u_pwd;
	private String u_last_name;
	private String u_first_name;
	private String u_residence;
	private String u_company;
	private String u_introduce;
	private String u_potoaddress;
	private String u_phone;
	private String u_date;
	private boolean activate;
	private boolean admin;
	public String getU_id() {
		return u_id;
	}
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}
	public String getU_pwd() {
		return u_pwd;
	}
	public void setU_pwd(String u_pwd) {
		this.u_pwd = u_pwd;
	}
	public String getU_last_name() {
		return u_last_name;
	}
	public void setU_last_name(String u_last_name) {
		this.u_last_name = u_last_name;
	}
	public String getU_first_name() {
		return u_first_name;
	}
	public void setU_first_name(String u_first_name) {
		this.u_first_name = u_first_name;
	}
	public String getU_residence() {
		return u_residence;
	}
	public void setU_residence(String u_residence) {
		this.u_residence = u_residence;
	}
	public String getU_company() {
		return u_company;
	}
	public void setU_company(String u_company) {
		this.u_company = u_company;
	}
	public String getU_introduce() {
		return u_introduce;
	}
	public void setU_introduce(String u_introduce) {
		this.u_introduce = u_introduce;
	}
	public String getU_potoaddress() {
		return u_potoaddress;
	}
	public void setU_potoaddress(String u_potoaddress) {
		this.u_potoaddress = u_potoaddress;
	}
	public String getU_date() {
		return u_date;
	}
	public void setU_date(String u_date) {
		this.u_date = u_date;
	}
	public boolean isActivate() {
		return activate;
	}
	public void setActivate(boolean activate) {
		this.activate = activate;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getU_phone() {
		return u_phone;
	}
	public void setU_phone(String u_phone) {
		this.u_phone = u_phone;
	}
}
