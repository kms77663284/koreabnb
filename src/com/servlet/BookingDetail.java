package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.BookingDAO;
import com.dao.HotelDAO;
import com.dao.UserDAO;
import com.dto.Booking;
import com.dto.Hotel;
import com.dto.User;


@WebServlet("/BookingDetail")
public class BookingDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public BookingDetail() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int b_idx = Integer.parseInt(request.getParameter("b_idx"));
		String room="";
		String detail="";
		String booking ="";
		room="display: none;";
		detail="display: block;";
		booking="display: none;";
		request.setAttribute("room", room);
		request.setAttribute("detail",detail);
		request.setAttribute("booking", booking);
		Booking bInfo = new Booking();
		BookingDAO dao = BookingDAO.getInstance();
		bInfo = dao.getBooking(b_idx);
		
		UserDAO u_dao = UserDAO.getInstance();
		HotelDAO h_dao = HotelDAO.getInstance();
		
		User u =u_dao.getUser(bInfo.getB_uid());
		bInfo.setB_uname(u.getU_first_name()+u.getU_last_name());
		Hotel h=h_dao.selectHotelInfo(Integer.toString(bInfo.getB_hidx()));
		request.setAttribute("bInfo", bInfo);
		request.setAttribute("hotel", h);
		RequestDispatcher dispatcher = null;
		dispatcher = request.getRequestDispatcher("HostMyPage.jsp");


		dispatcher.forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
