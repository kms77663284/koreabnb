package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.NoticeDAO;
import com.dto.Notice;

@WebServlet("/notice_del")
public class notice_del extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public notice_del() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		NoticeDAO ndo = NoticeDAO.getInstance();
		
		int n_idx = Integer.parseInt((String)request.getParameter("n_idx").toString());
		
		Notice delNotice = new Notice();
		delNotice.setN_idx(n_idx);
		ndo.delNotice(delNotice);
		
		response.sendRedirect("Admin/notice.jsp");
		
		
	}

}
