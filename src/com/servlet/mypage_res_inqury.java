package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.BookingDAO;
import com.dao.HotelDAO;
import com.dao.UserDAO;
import com.dto.Booking;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class mypage_res_inqury
 */
@WebServlet("/mypage_res_inqury")
public class mypage_res_inqury extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public mypage_res_inqury() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("user")==null) {response.sendRedirect("Login.jsp");}
		String id = request.getParameter("id");
		BookingDAO bao = BookingDAO.getInstance();
		
		Booking data = bao.getBooking(Integer.parseInt(id));
		request.setAttribute("booking", data);
		
		HotelDAO hdo = HotelDAO.getInstance();
		Hotel hotel = hdo.selectHotelInfo(Integer.toString(data.getB_hidx()));
		request.setAttribute("hotel", hotel);
		
		UserDAO uao = UserDAO.getInstance();
		User host = uao.getUser(data.getB_hid());
		request.setAttribute("host", host);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("mypage_res_inqury.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
