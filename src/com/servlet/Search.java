package com.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.HotelDAO;
import com.dto.Hotel;
import com.dao.HotelDAO;
/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HotelDAO hdo = HotelDAO.getInstance();
		String search = request.getParameter("search");
		String dateStart = request.getParameter("dateStart");
		String dateEnd = request.getParameter("dateEnd");
		String guests = request.getParameter("guests");
		String type1 = request.getParameter("type1");
		String price = request.getParameter("price");
		int start = Integer.parseInt(request.getParameter("page"));
		int cut= 5;
		
		int hotelnum = hdo.getHotelCount(search, dateStart, dateEnd, guests, type1, price);
		request.setAttribute("hotelnum", hotelnum);
		
		
		ArrayList<Hotel> list = hdo.getHotelList(search, dateStart, dateEnd, guests, type1, price, start, cut);
		request.setAttribute("list", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("SearchPage.jsp");
		dispatcher.forward(request, response);
	}
}
