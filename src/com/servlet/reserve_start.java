package com.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.Hotel;
import com.dao.HotelDAO;

/**
 * Servlet implementation class reserve_start
 */
@WebServlet("/reserve_start")
public class reserve_start extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public reserve_start() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String ppl =request.getParameter("ppl");
		HotelDAO d = HotelDAO.getInstance();
		
		Hotel hotel = d.selectHotelInfo(request.getParameter("h_idx"));
		String checkin = request.getParameter("checkin");
		String checkout = request.getParameter("checkout");
		System.out.println(checkin + " " +checkout);
		request.setAttribute("hotel", hotel);
		long calDateDays = 0;
		try {
		
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			Date chin = format.parse(checkin);
			Date chout = format.parse(checkout);
			
			long calDate = chout.getTime() - chin.getTime();
			calDateDays = calDate / (24*60*60*1000);
			System.out.println(calDateDays+"박" +"  호텔"+ hotel.getH_idx());
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("date", calDateDays);
		request.setAttribute("ppl", ppl);
		request.setAttribute("checkin", checkin);
		request.setAttribute("checkout", checkout);
		
		
		RequestDispatcher dispatcher = null;
		dispatcher = request.getRequestDispatcher("reserve.jsp");
		dispatcher.forward(request, response);
	}

}
