package com.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.HoteltypeDAO;
import com.dao.RuleDAO;

/**
 * Servlet implementation class adminproperty
 */
@WebServlet("/adminproperty")
public class adminproperty extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminproperty() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RuleDAO rdo = RuleDAO.getInstance();
		HoteltypeDAO hdo = HoteltypeDAO.getInstance();
		ArrayList<String>[] list = new ArrayList[3];
		list[0] = hdo.GetType1List();
		list[1] = hdo.GetType2List();
		list[2] = (ArrayList<String>) rdo.selectRules();
		request.setAttribute("list", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Admin/property.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String met = request.getParameter("met");
		if (met.equals("add")) {
			String type = request.getParameter("type");
			if (type.equals("rules")) {
				RuleDAO rdo = RuleDAO.getInstance();
				rdo.SetRule(request.getParameter("item"));
			}
			else {
				HoteltypeDAO hdo = HoteltypeDAO.getInstance();
				if (type.equals("type1")) hdo.SetType1(request.getParameter("item"));
				else hdo.SetType2(request.getParameter("item"));
			}
		}
		else {
			String type = request.getParameter("type");
			if (type.equals("rules")) {
				RuleDAO rdo = RuleDAO.getInstance();
				rdo.DelRule(request.getParameter("item"));
			}
			else {
				HoteltypeDAO hdo = HoteltypeDAO.getInstance();
				if (type.equals("type1")) hdo.DelType1(request.getParameter("item"));
				else hdo.DelType2(request.getParameter("item"));
			}
		}
	}

}
