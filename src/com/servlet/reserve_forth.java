package com.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.BookingDAO;
import com.dao.HotelDAO;
import com.dto.Booking;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class reserve_forth
 */
@WebServlet("/reserve_forth")
public class reserve_forth extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public reserve_forth() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		BookingDAO dao = BookingDAO.getInstance();
		User user = (User)request.getSession().getAttribute("user");
		HotelDAO h_dao =HotelDAO.getInstance();
		Hotel hotel = h_dao.selectHotelInfo(request.getParameter("h_idx"));
		Booking b = new Booking();
		b.setB_checkin(request.getParameter("b_checkin"));
		b.setB_checkout(request.getParameter("b_checkout"));
		b.setB_hidx(Integer.parseInt(request.getParameter("h_idx")));
		b.setB_uid(user.getU_id());
		b.setB_card(request.getParameter("b_card"));
		b.setB_card_info(request.getParameter("b_card_info"));
		b.setB_deposit(true);
		b.setB_guests(Integer.parseInt(request.getParameter("ppl")));
		b.setB_hid(hotel.getH_id());
		b.setB_htitle(hotel.getH_title());
		b.setB_price(Integer.parseInt(request.getParameter("price")));
		b.setB_msg(request.getParameter("b_msg"));
		RequestDispatcher dispatcher = null;
		dao.addBooking(b);
		System.out.println("예약완료");
		System.out.println(b.getB_checkin()+" " +b.getB_checkout() + "체크인 체크아웃 ");
		String reseDate = dateLogic(b.getB_checkin(),b.getB_checkout());
		h_dao.updatePossible(reseDate,Integer.parseInt(request.getParameter("h_idx")),hotel.getH_res_not_possible_date());		
		
		
		dispatcher = request.getRequestDispatcher("mypage_res?page=1");
		dispatcher.forward(request, response);
	}


	private String dateLogic(String in, String out) { //체크인 체크아웃 사이 날짜 계산
		String dateStr="";
		try {
			final String DATE_PATTERN = "yyyy-MM-dd";
	        
	        SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
	        Date startDate = sdf.parse(in);
	        Date endDate =sdf.parse(out);;

		
	        ArrayList<String> dates = new ArrayList<String>();
	        Date currentDate = startDate;
	        while (currentDate.compareTo(endDate) <= 0) {
	            dates.add(sdf.format(currentDate));
	            Calendar c = Calendar.getInstance();
	            c.setTime(currentDate);
	            c.add(Calendar.DAY_OF_MONTH, 1);
	            currentDate = c.getTime();
	        }
	        for ( int i=0;i<dates.size();i++) {
	        	
	            dateStr += dates.get(i) + ",";
	        }
	        dateStr = dateStr.substring(0,dateStr.length()-1); //뒷자리 하나 삭제  ,요거 삭제
	        System.out.println("예약차단기간 :"+dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return dateStr;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
