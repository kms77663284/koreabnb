package com.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.HotelDAO;
import com.dto.Hotel;



/**
 * Servlet implementation class HotelInfoAction
 */
@WebServlet("/HotelInfoAction")
public class HotelInfoAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HotelInfoAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprosess(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprosess(request,response);
	}

	private void doprosess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		
		String h_idx=(String)  request.getAttribute("h_idx");
		if(h_idx == null) {//마이페이지에서 왔다면
			h_idx = request.getParameter("h_idx");
		}

		HotelDAO dao = HotelDAO.getInstance();
		Hotel hotel = new Hotel();
		
		hotel = dao.selectHotelInfo(h_idx);
		request.setAttribute("hotelinfo", hotel);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("HostModifivation.jsp");

		dispatcher.forward(request, response);
		
		
	
	}

}
