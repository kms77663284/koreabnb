package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.NoticeDAO;
import com.dto.Notice;

@WebServlet("/notice_mod")
public class notice_mod extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public notice_mod() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String n_title = request.getParameter("n_title");
		String n_content = request.getParameter("n_content");
		int n_idx = Integer.parseInt(request.getParameter("n_idx"));
		
		NoticeDAO ndo = NoticeDAO.getInstance();
		
		Notice tmp = new Notice();
		tmp.setN_idx(n_idx);
		tmp.setN_title(n_title);
		tmp.setN_content(n_content);
		ndo.modNotice(tmp);
		
		response.sendRedirect("Admin/notice.jsp");
	}

}
