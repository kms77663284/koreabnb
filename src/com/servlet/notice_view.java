package com.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.NoticeDAO;
import com.dto.Notice;

@WebServlet("/notice_view")
public class notice_view extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public notice_view() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int n_idx = Integer.parseInt(request.getParameter("n_idx").toString());
		NoticeDAO ndo = NoticeDAO.getInstance();
		
		Notice noticeView = ndo.viewNotice(n_idx);
		request.setAttribute("noticeView", noticeView);
	
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Admin/notice_view.jsp");
		dispatcher.forward(request, response);		
		
	}

}
