package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDAO;
import com.dto.User;

/**
 * Servlet implementation class UpdatePrivacy
 */
@WebServlet("/UpdatePrivacy")
public class UpdatePrivacy extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdatePrivacy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDAO uao = UserDAO.getInstance();
		String sel = request.getParameter("sel");
		String val = request.getParameter("val");
		
		User user = (User)request.getSession().getAttribute("user");
		if (user != null) {
			if(sel.equals("1")) { // name
				String[] names = val.split(" ");
				if (names.length == 2) {
					user.setU_first_name(names[0]);
					user.setU_last_name(names[1]);
				}
			}
			if(sel.equals("2")) { // phone
				user.setU_phone(val);
			}
			if(sel.equals("3")) { // address
				user.setU_residence(val);
			}
			uao.UpdateUser(user);
		}else {
			response.sendRedirect("Login.jsp");
		}
	}

}
