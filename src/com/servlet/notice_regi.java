package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.NoticeDAO;
import com.dto.Notice;
import com.dto.User;


@WebServlet("/notice_regi")
public class notice_regi extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public notice_regi() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		NoticeDAO ndo = NoticeDAO.getInstance();
		
		String title = request.getParameter("n_title");
		String content = request.getParameter("n_content");
		
		User user = (User)request.getSession().getAttribute("user");
		
		Notice tmp = new Notice();
		tmp.setN_title(title);
		tmp.setN_content(content);
		tmp.setA_id(user.getU_id());
		ndo.addNotice(tmp);
	
		response.sendRedirect("Admin/notice.jsp");
	}

}
