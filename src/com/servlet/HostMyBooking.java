package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.BookingDAO;
import com.dao.HotelDAO;
import com.dto.Booking;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class HostMyBooking
 */
@WebServlet("/HostMyBooking")
public class HostMyBooking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public HostMyBooking() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProsess(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProsess(request,response);
	}

	private void doProsess(HttpServletRequest request, HttpServletResponse response) {
		try {
		RequestDispatcher dispatcher = null;
		//String url = request.getRequestURL().substring((request.getRequestURL().lastIndexOf("/")));
		

		
		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("user");
		List<Booking> list = new ArrayList<Booking>();
		//String referer = request.getHeader("referer");
		//referer = referer.substring(referer.lastIndexOf("/")).substring(1); //무슨페이지에서 왔는지
		//System.out.println(referer);
		String room="";
		String detail="";
		String booking ="";
		
		//if(referer.equalsIgnoreCase("HostModifivation.jsp")||referer.equalsIgnoreCase("HotelAddAction")||referer.equalsIgnoreCase("HotelAddEdit")) {
			room="display: none;";
			detail="display: none;";
			booking="display: block;";
			request.setAttribute("room", room);
			request.setAttribute("detail",detail);
			request.setAttribute("booking", booking);
			BookingDAO dao = BookingDAO.getInstance();
			
			list = dao.selectBookingList(u.getU_id());
			request.setAttribute("BookingList", list);
			dispatcher = request.getRequestDispatcher("HostMyPage.jsp");
		//}

			dispatcher.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
