package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.QuestionAdDAO;
import com.dto.Question;

@WebServlet("/question_ans")
public class question_ans extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public question_ans() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String q_answer = request.getParameter("q_answer");
		int q_idx = Integer.parseInt(request.getParameter("q_idx"));
		
		
		QuestionAdDAO dao = QuestionAdDAO.getInstance();
		
		Question question = new Question();
		question.setQ_answer(q_answer);
		question.setQ_idx(q_idx);
		dao.answerQuestion(question);
		
		response.sendRedirect("Admin/question.jsp");
		
	}

}
