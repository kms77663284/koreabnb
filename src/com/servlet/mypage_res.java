package com.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.BookingDAO;
import com.dto.Booking;
import com.dto.User;

/**
 * Servlet implementation class mypage_res
 */
@WebServlet("/mypage_res")
public class mypage_res extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public mypage_res() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect("Login.jsp");
		}
		BookingDAO bao = BookingDAO.getInstance();
		int page = Integer.parseInt(request.getParameter("page"));
		
		ArrayList<Booking> list = (ArrayList<Booking>)bao.selectBookingList(user.getU_id(), page, 5);
		request.setAttribute("list", list);
		
		int bookingnum = bao.CountBooking(user.getU_id());
		request.setAttribute("bookingnum", bookingnum);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("mypage_res.jsp?page="+page);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
