package com.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.HotelDAO;
import com.dao.ReviewDAO;
import com.dao.UserDAO;
import com.dto.Hotel;
import com.dto.Review;
import com.dto.User;

/**
 * Servlet implementation class reviewDelete
 */
@WebServlet("/reviewDelete")
public class reviewDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public reviewDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String r_idx = request.getParameter("r_idx");
		String h_idx = request.getParameter("h_idx");
		HotelDAO dao = HotelDAO.getInstance();
		ReviewDAO r_dao = ReviewDAO.getInstance();
		UserDAO u_dao = UserDAO.getInstance();
		
		
		Hotel hotel = dao.selectHotelInfo(h_idx);
		ArrayList<Review> review = new ArrayList<Review>();
		r_dao.delReview(r_idx);
		
		review=r_dao.getReviews(h_idx);
		User h_user_info = u_dao.getUser(hotel.getH_id());
		
		
		request.setAttribute("h_user_info", h_user_info);
		
		request.setAttribute("hotel", hotel);
		if(review.size()>0) {
			request.setAttribute("review", review);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("Detail.jsp");
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
