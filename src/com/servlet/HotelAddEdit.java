package com.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import com.dao.HotelDAO;
import com.dto.Hotel;
import com.dto.User;

/**
 * Servlet implementation class HotelAddEdit
 */
@WebServlet("/HotelAddEdit")
public class HotelAddEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HotelAddEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProsess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProsess(request, response);
	}

	private void doProsess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		Hotel hotel = new Hotel();
		String step = request.getParameter("step");
		RequestDispatcher dispatcher = null;
		User u = (User)session.getAttribute("user");
		
		
		if(step.equals("1")) {//1단계
			System.out.println("1단계");
			
			hotel.setH_id(u.getU_id());
			hotel.setH_idx(Integer.parseInt(request.getParameter("h_idx")));
			hotel.setH_address1(request.getParameter("h_address1"));
			hotel.setH_address2(request.getParameter("h_address2"));
			hotel.setH_address3(request.getParameter("h_address3"));
			hotel.setH_address4(request.getParameter("h_address4"));
			hotel.setH_zipcode(request.getParameter("h_zipcode"));
			hotel.setH_guests(Integer.parseInt(request.getParameter("h_guests")));
			hotel.setH_type(request.getParameter("h_type"));
			hotel.setH_type2(request.getParameter("h_type2"));
			hotel.setH_mapX(Double.parseDouble(request.getParameter("h_mapX")));
			hotel.setH_mapY(Double.parseDouble(request.getParameter("h_mapY")));
			String faclist[]= {};
			if(request.getParameterValues("h_facilities")!=null) {
				faclist =request.getParameterValues("h_facilities");
			}
			
			String fac ="";
			if(faclist.length>0) {
				for(int i=0;i<faclist.length;i++) {
					if(i < faclist.length-1){
						fac += faclist[i]+",";
					}else {
						fac += faclist[i];
					}
				}
			}
			hotel.setH_facilities(fac);
			
			HotelDAO dao = HotelDAO.getInstance();
			
			
			dao.onestepadd(hotel);
			hotel = dao.selectHotelInfo(request.getParameter("h_idx"));

			
	
		}else if (step.equals("2")) { //2단계
			HotelDAO dao = HotelDAO.getInstance();
		
			hotel.setH_idx(Integer.parseInt(request.getParameter("h_idx")));
			hotel.setH_introduce(request.getParameter("h_introduce"));
			hotel.setH_other(request.getParameter("h_other"));
			hotel.setH_title(request.getParameter("h_title"));
			
			
			dao.twostepadd(hotel);
			hotel = dao.selectHotelInfo(request.getParameter("h_idx"));
		
		}else { //3단계
			HotelDAO dao = HotelDAO.getInstance();
			
			hotel.setH_idx(Integer.parseInt(request.getParameter("h_idx")));
			hotel.setH_price(Integer.parseInt(request.getParameter("h_price")));
			String res = request.getParameter("h_res_not_possible_date");
			if(res.equalsIgnoreCase("null")) {
				res ="";
			}else {
				res = res.substring(0,res.length()-1);
			}
			
			hotel.setH_res_not_possible_date(res);
			String ruleslist[] = request.getParameterValues("h_rules");
			String rules ="";
			for(int i=0;i<ruleslist.length;i++) {
				if(i < ruleslist.length-1){
					rules += ruleslist[i]+",";
				}else {
					rules += ruleslist[i];
				}
			}
			
			hotel.setH_rules(rules);
			
			System.out.println(hotel.getH_res_not_possible_date());
			System.out.println(hotel.getH_rules());
			System.out.println(hotel.getH_price());
			
			
			dao.threestepadd(hotel);
			hotel = dao.selectHotelInfo(request.getParameter("h_idx"));
		
		}
		
		request.setAttribute("hotelinfo", hotel);
		dispatcher = request.getRequestDispatcher("HostModifivation.jsp");
		dispatcher.forward(request, response);
		
		
	}

}
