package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dto.Booking;
import com.dto.Hotel;

public class BookingDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static BookingDAO instance;
	
	private BookingDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static BookingDAO getInstance() {
		if (instance == null) instance = new BookingDAO();
		return instance;
	}
	public List<Booking> selectBookingList(String u_id) {
		List<Booking> list = new ArrayList<Booking>();
		sql="select * from booking where h_id=?";
		try {
			HotelDAO dao = HotelDAO.getInstance();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u_id);
			
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Booking booking = new Booking();
				booking.setB_idx(rs.getInt("b_idx"));
				booking.setB_checkin(rs.getString("b_checkin"));
				booking.setB_checkout(rs.getString("b_checkout"));
				booking.setB_date(rs.getString("b_date"));
				booking.setB_deposit(rs.getBoolean("b_deposit"));
				booking.setB_guests(rs.getInt("b_guests"));
				booking.setB_hid(rs.getString("h_id"));
				booking.setB_hidx(rs.getInt("h_idx"));
				booking.setB_msg(rs.getString("b_msg"));
				booking.setB_price(rs.getInt("b_price"));
				booking.setB_uid(rs.getString("u_id"));
				
				
				Hotel hotel = new Hotel();
				
				hotel = dao.selectHotelInfo(Integer.toString(rs.getInt("h_idx")));
				
				booking.setB_htitle(hotel.getH_title());
				
				list.add(booking);
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
		
		return list;
	}
	public void addBooking(Booking b) {
		try {
			
			sql = "Insert into booking(h_idx,u_id,h_id,b_checkin,b_checkout,b_price,b_deposit,b_guests,b_msg,b_card,b_card_info)"
					+ " value(?,?,?,?,?,?,?,?,?,?,?)";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, b.getB_hidx());
			pstmt.setString(2, b.getB_uid());
			pstmt.setString(3, b.getB_hid());
			pstmt.setString(4, b.getB_checkin());
			pstmt.setString(5, b.getB_checkout());
			pstmt.setInt(6, b.getB_price());
			pstmt.setBoolean(7, b.isB_deposit());
			pstmt.setInt(8, b.getB_guests());
			pstmt.setString(9, b.getB_msg());
			pstmt.setString(10, b.getB_card());
			pstmt.setString(11, b.getB_card_info());
			
			
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	// 마이페이지용 예약 목록 호출, 페이징 효과 겸함
	public List<Booking> selectBookingList(String u_id, int page, int cut){
		ArrayList<Booking> list = new ArrayList<Booking>();
		try {
			HotelDAO dao = HotelDAO.getInstance();
			sql = "SELECT b_idx, b_checkin, b_checkout, b_date, b_deposit, "
					+ "b_guests, h_id, h_idx, b_msg, b_price, u_id "
					+ "FROM booking WHERE u_id=? limit ?, ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u_id);
			pstmt.setInt(2, (page-1)*cut);
			pstmt.setInt(3, cut);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Booking booking = new Booking();
				booking.setB_idx(rs.getInt("b_idx"));
				booking.setB_checkin(rs.getString("b_checkin"));
				booking.setB_checkout(rs.getString("b_checkout"));
				booking.setB_date(rs.getString("b_date"));
				booking.setB_deposit(rs.getBoolean("b_deposit"));
				booking.setB_guests(rs.getInt("b_guests"));
				booking.setB_hid(rs.getString("h_id"));
				booking.setB_hidx(rs.getInt("h_idx"));
				booking.setB_msg(rs.getString("b_msg"));
				booking.setB_price(rs.getInt("b_price"));
				booking.setB_uid(rs.getString("u_id"));
				
				Hotel hotel = new Hotel();
				hotel = dao.selectHotelInfo(Integer.toString(rs.getInt("h_idx")));
				booking.setB_htitle(hotel.getH_title());
				StringBuffer address = new StringBuffer();
				if (hotel.getH_address1() != null && !hotel.getH_address1().equals("")) {
					address.append(hotel.getH_address1());
					if (hotel.getH_address2() != null && !hotel.getH_address2().equals("")) {
						address.append(" ");
						address.append(hotel.getH_address2());
						if (hotel.getH_address3() != null && !hotel.getH_address3().equals("")) {
							address.append(" ");
							address.append(hotel.getH_address3());
							if (hotel.getH_address4() != null && !hotel.getH_address4().equals("")) {
								address.append(" ");
								address.append(hotel.getH_address4());
							}
						}
					}
				}
				
				booking.setB_address(address.toString());
				
				list.add(booking);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	// 예약 갯수 확인
	public int CountBooking(String u_id) {
		int result=0;
		try {
			sql = "SELECT count(b_idx) FROM booking WHERE u_id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u_id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = rs.getInt(1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	// 관리자페이지 예약 목록 호출, 검색 + 페이징 기능 추가
	public ArrayList<Booking> getBookingList(String pageNum, String search, String catg) {
		ArrayList<Booking> bookingList = new ArrayList<Booking>();
		
		try {
			if(catg.equals("b_idx")) {
				sql = "SELECT * FROM booking WHERE b_idx LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			} else if(catg.equals("h_id")) {
				sql = "SELECT * FROM booking WHERE h_id LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("u_id")) {
				sql = "SELECT * FROM booking WHERE u_id LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("b_date")) {
				sql = "SELECT * FROM booking WHERE b_date LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("b_deposit") && search.equals("Y")) {
				sql = "SELECT * FROM booking WHERE b_deposit LIKE TRUE ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("b_deposit") && search.equals("N")) {
				sql = "SELECT * FROM booking WHERE b_deposit LIKE FALSE ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("b_checkin")) {
				sql = "SELECT * FROM booking WHERE b_checkin LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("b_checkout")) {
				sql = "SELECT * FROM booking WHERE b_checkout LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else if(catg.equals("") && search != "") {
				sql = "SELECT * FROM booking WHERE b_idx LIKE '%"+search+"%' OR h_id LIKE '%"+search+"%' OR u_id LIKE '%"+search+"%' OR b_date LIKE '%"+search+"%' OR b_checkin LIKE '%"+search+"%' OR b_checkout LIKE '%"+search+"%' ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}else {
				sql = "SELECT * FROM booking ORDER BY b_idx DESC LIMIT ? , 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					Booking booking = new Booking();
					booking.setB_idx(rs.getInt("b_idx"));
					booking.setB_hidx(rs.getInt("h_idx"));
					booking.setB_hid(rs.getString("h_id"));
					booking.setB_uid(rs.getString("u_id"));
					booking.setB_date(rs.getString("b_date"));
					booking.setB_deposit(rs.getInt("b_deposit")==1?true:false);
					booking.setB_checkin(rs.getString("b_checkin"));
					booking.setB_checkout(rs.getString("b_checkout"));				
					bookingList.add(booking);				
				} 
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}		
		return bookingList;
	}
	
	public int targetPage(String pageNum, String search, String catg) {
		try {
			if(catg.equals("b_idx")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_idx LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if(catg.equals("h_id")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE h_id LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("u_id")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE u_id LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("b_date")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_date LIKE '%"+search+"%' ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("b_deposit") && search.equals("Y")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_deposit LIKE TRUE ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("b_deposit") && search.equals("N")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_deposit LIKE FALSE ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("b_checkin")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_checkin LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("b_checkout")) {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_checkout LIKE '%"+search+"%' ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("") && search != "") {
				sql = "SELECT COUNT(b_idx) FROM booking WHERE b_idx LIKE '%"+search+"%' OR h_id LIKE '%"+search+"%' OR u_id LIKE '%"+search+"%' OR b_date LIKE '%"+search+"%' OR b_checkin LIKE '%"+search+"%' OR b_checkout LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else {
				sql = "SELECT COUNT(b_idx) FROM booking";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				} 
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}			
		return 0;
	}


	// 예약 1개 내용 확인
	public Booking getBooking(int b_idx) {
		Booking booking = null;
		try {
			sql = "SELECT b_idx, b_checkin, b_checkout, b_date, b_deposit, b_guests, h_id, h_idx, b_msg, b_price, u_id FROM booking WHERE b_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,b_idx);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				booking = new Booking();
				booking.setB_idx(rs.getInt("b_idx"));
				booking.setB_checkin(rs.getString("b_checkin"));
				booking.setB_checkout(rs.getString("b_checkout"));
				booking.setB_date(rs.getString("b_date"));
				booking.setB_deposit(rs.getBoolean("b_deposit"));
				booking.setB_guests(rs.getInt("b_guests"));
				booking.setB_hid(rs.getString("h_id"));
				booking.setB_hidx(rs.getInt("h_idx"));
				booking.setB_msg(rs.getString("b_msg"));
				booking.setB_price(rs.getInt("b_price"));
				booking.setB_uid(rs.getString("u_id"));				
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return booking;
	}
}
