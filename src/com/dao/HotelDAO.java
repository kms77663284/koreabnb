package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.dto.Hotel;
import com.dto.User;

public class HotelDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static HotelDAO instance;
	
	private HotelDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static HotelDAO getInstance() {
		if (instance == null) instance = new HotelDAO();
		return instance;
	}
//-----------------------------------------------------------------------------------------------------------------------------------------

//본인의숙소 리스트 가져오기
		public List<Hotel> HotelList(String id){
			if(conn == null) {
				conn = ConnectionDAO.getConnection();
			}
			String sql="select * from hotelinfo where h_id=?";
			List<Hotel> list = new ArrayList<Hotel>();
			ResultSet rs= null;
			
			try {
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
			
				rs = pstmt.executeQuery();
	
				while(rs.next()) {
					Hotel hotel = new Hotel();
					hotel.setH_id(rs.getString("h_id"));
					hotel.setH_address1(rs.getString("h_address1"));
					hotel.setH_address2(rs.getString("h_address2"));
					hotel.setH_address3(rs.getString("h_address3"));
					hotel.setH_address4(rs.getString("h_address4"));
					hotel.setH_date(rs.getString("h_date"));
					hotel.setH_facilities(rs.getString("h_facilities"));
					hotel.setH_grant(rs.getBoolean("h_grant"));
					hotel.setH_guests(rs.getInt("h_guests"));
					hotel.setH_introduce(rs.getString("h_introduce"));
					hotel.setH_other(rs.getString("h_other"));
					hotel.setH_price(rs.getInt("h_price"));
					//hotel.setH_potos(rs.getString("h_potos"));
					hotel.setH_res_not_possible_date(rs.getString("h_res_not_possible_date"));
					hotel.setH_rules(rs.getString("h_rules"));
					hotel.setH_star(rs.getFloat("h_star"));
					hotel.setH_title(rs.getString("h_title"));
					hotel.setH_type(rs.getString("h_type"));
					hotel.setH_type2(rs.getString("h_type2"));
					hotel.setH_zipcode(rs.getString("h_zipcode"));
					hotel.setH_idx(rs.getInt("h_idx"));
					hotel.setH_mapX(rs.getDouble("h_mapX"));
					hotel.setH_mapY(rs.getDouble("h_mapY"));
					list.add(hotel);
				}
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return list;
		}
		//하나의 호텔 정보가져오기
		public Hotel selectHotelInfo(String h_idx) {
			Hotel hotel = new Hotel();
			if(conn == null) {
				conn = ConnectionDAO.getConnection();
			}
			String sql="select * from hotelinfo where h_idx=?";
			ResultSet rs= null;
			try {
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, h_idx);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					hotel.setH_id(rs.getString("h_id"));
					hotel.setH_address1(rs.getString("h_address1"));
					hotel.setH_address2(rs.getString("h_address2"));
					hotel.setH_address3(rs.getString("h_address3"));
					hotel.setH_address4(rs.getString("h_address4"));
					hotel.setH_date(rs.getString("h_date"));
					hotel.setH_facilities(rs.getString("h_facilities"));
					hotel.setH_grant(rs.getBoolean("h_grant"));
					hotel.setH_guests(rs.getInt("h_guests"));
					hotel.setH_introduce(rs.getString("h_introduce"));
					hotel.setH_other(rs.getString("h_other"));
					hotel.setH_price(rs.getInt("h_price"));
					//hotel.setH_potos(rs.getString("h_potos"));
					hotel.setH_res_not_possible_date(rs.getString("h_res_not_possible_date"));
					hotel.setH_rules(rs.getString("h_rules"));
					hotel.setH_star(rs.getFloat("h_star"));
					hotel.setH_title(rs.getString("h_title"));
					hotel.setH_type(rs.getString("h_type"));
					hotel.setH_type2(rs.getString("h_type2"));
					hotel.setH_zipcode(rs.getString("h_zipcode"));
					hotel.setH_idx(rs.getInt("h_idx"));
					hotel.setH_mapX(rs.getDouble("h_mapX"));
					hotel.setH_mapY(rs.getDouble("h_mapY"));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			return hotel;
		}

		public void onestepadd(Hotel hotel) {//1단계 추가 & 업데이트

			try {
				sql = "update hotelinfo set h_type=?,h_type2=?,h_guests=?,h_facilities=?,h_address1=?,h_address2=?,h_address3=?,h_address4=?,h_zipcode=?,h_mapX=?,h_mapY=? where h_idx=?";
				pstmt=conn.prepareStatement(sql);
				pstmt.setString(1, hotel.getH_type());
				pstmt.setString(2, hotel.getH_type2());
				pstmt.setInt(3, hotel.getH_guests());
				pstmt.setString(4, hotel.getH_facilities());
				pstmt.setString(5, hotel.getH_address1());
				pstmt.setString(6, hotel.getH_address2());
				pstmt.setString(7, hotel.getH_address3());
				pstmt.setString(8, hotel.getH_address4());
				pstmt.setString(9, hotel.getH_zipcode());
				pstmt.setDouble(10, hotel.getH_mapX());
				pstmt.setDouble(11, hotel.getH_mapY());
				
				pstmt.setInt(12, hotel.getH_idx());
				
				pstmt.executeUpdate();
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}

		public String insertHotel(String id) {
			sql = "insert into hotelinfo (h_id) values (?)";
			String h_idx="";
			try {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
				pstmt.executeUpdate();
				
				sql = "select * from hotelinfo where h_id=? order by h_idx desc";
				pstmt =conn.prepareStatement(sql);
				pstmt.setString(1, id);
				
				rs = pstmt.executeQuery();
				
				if(rs.next()) {
					h_idx = rs.getString("h_idx");
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return h_idx;
		}

		public void twostepadd(Hotel hotel) {
			System.out.println(hotel.getH_title());
			
			try {
				sql = "update hotelinfo set h_title=?,h_introduce=?,h_other=? where h_idx=?";
				pstmt=conn.prepareStatement(sql);
				pstmt.setString(1, hotel.getH_title());
				pstmt.setString(2, hotel.getH_introduce());
				pstmt.setString(3, hotel.getH_other());
				pstmt.setInt(4, hotel.getH_idx());
				
				pstmt.executeUpdate();
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void threestepadd(Hotel hotel) {
			
			
			try {
				sql = "update hotelinfo set h_rules=?,h_res_not_possible_date=?,h_price=? where h_idx=?";
				pstmt=conn.prepareStatement(sql);
				pstmt.setString(1, hotel.getH_rules());
				pstmt.setString(2, hotel.getH_res_not_possible_date());
				pstmt.setInt(3, hotel.getH_price());
				pstmt.setInt(4, hotel.getH_idx());
				
				pstmt.executeUpdate();
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	






//--------------------------------------------------------------------------------------------------------------------------------------------	
	public Hotel getHotel(int h_idx) {
		Hotel hotel = null;
		try {
			sql = "SELECT * FROM hotelinfo WHERE h_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, h_idx);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				hotel = new Hotel();
				hotel.setH_idx(rs.getInt("h_idx"));
				hotel.setH_id(rs.getString("h_id"));
				hotel.setH_address1(rs.getString("h_address1"));
				hotel.setH_address2(rs.getString("h_address2"));
				hotel.setH_address3(rs.getString("h_address3"));
				hotel.setH_address4(rs.getString("h_address4"));
				hotel.setH_zipcode(rs.getString("h_zipcode"));
				hotel.setH_grant(rs.getInt("h_grant")==1?true:false);
				hotel.setH_guests(rs.getInt("h_guests"));
				hotel.setH_price(rs.getInt("h_price"));
				hotel.setH_title(rs.getString("h_title"));
				hotel.setH_type(rs.getString("h_type"));
				hotel.setH_type2(rs.getString("h_type2"));
				hotel.setH_date(rs.getString("h_date"));
				hotel.setH_facilities(rs.getString("h_facilities"));
				hotel.setH_rules(rs.getString("h_rules"));
				hotel.setH_res_not_possible_date(rs.getString("h_res_not_possible_date"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return hotel;
	}
	
	public ArrayList<Hotel> getHotelList(){
		ArrayList<Hotel> list = null;
		try{
			list = new ArrayList<Hotel>();
			sql = "Select * from hotelinfo";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				Hotel tmp = new Hotel();
				tmp.setH_idx(rs.getInt("h_idx"));
				tmp.setH_id(rs.getString("h_id"));
				tmp.setH_address1(rs.getString("h_address1"));
				tmp.setH_address2(rs.getString("h_address2"));
				tmp.setH_address3(rs.getString("h_address3"));
				tmp.setH_address4(rs.getString("h_address4"));
				tmp.setH_zipcode(rs.getString("h_zipcode"));
				tmp.setH_grant(rs.getInt("h_grant")==1?true:false);
				tmp.setH_guests(rs.getInt("h_guests"));
				tmp.setH_price(rs.getInt("h_price"));
				tmp.setH_title(rs.getString("h_title"));
				tmp.setH_type(rs.getString("h_type"));
				tmp.setH_type2(rs.getString("h_type2"));
				tmp.setH_date(rs.getString("h_date"));
				tmp.setH_facilities(rs.getString("h_facilities"));
				tmp.setH_rules(rs.getString("h_rule"));
				tmp.setH_res_not_possible_date(rs.getString("h_res_not_possible"));
				list.add(tmp);
			}
			rs.close();
			pstmt.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<Hotel> getHotelList(User user){
		ArrayList<Hotel> list = null;
		try{
			list = new ArrayList<Hotel>();
			sql = "Select * from hotelinfo where h_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getU_id());
			rs = pstmt.executeQuery();
			while(rs.next()){
				Hotel tmp = new Hotel();
				tmp.setH_idx(rs.getInt("h_idx"));
				tmp.setH_id(rs.getString("h_id"));
				tmp.setH_address1(rs.getString("h_address1"));
				tmp.setH_address2(rs.getString("h_address2"));
				tmp.setH_address3(rs.getString("h_address3"));
				tmp.setH_address4(rs.getString("h_address4"));
				tmp.setH_zipcode(rs.getString("h_zipcode"));
				tmp.setH_grant(rs.getInt("h_grant")==1?true:false);
				tmp.setH_guests(rs.getInt("h_guests"));
				tmp.setH_price(rs.getInt("h_price"));
				tmp.setH_title(rs.getString("h_title"));
				tmp.setH_type(rs.getString("h_type"));
				tmp.setH_type2(rs.getString("h_type2"));
				tmp.setH_date(rs.getString("h_date"));
				tmp.setH_facilities(rs.getString("h_facilities"));
				tmp.setH_rules(rs.getString("h_rule"));
				tmp.setH_res_not_possible_date(rs.getString("h_res_not_possible"));
				list.add(tmp);
			}
			rs.close();
			pstmt.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<Hotel> getHotelList(String search, String dateStart, String dateEnd, String guests, String type1, String price, int start, int cut){
		ArrayList<Hotel> list = new ArrayList<Hotel>();
		try {
			sql = "SELECT h_idx, h_mapX, h_mapY, h_address1, h_address2, h_type, h_type2, h_star, h_title, h_guests,h_facilities FROM hotelinfo ";
			StringBuffer buf = new StringBuffer(sql);
			boolean bf = false;
			
			if (search != null && search.length() > 0) {
				buf.append("WHERE (h_address1 LIKE '%");
				buf.append(search);
				buf.append("%' OR h_address2 LIKE '%");
				buf.append(search);
				buf.append("%' OR h_address3 LIKE '%");
				buf.append(search);
				buf.append("%' OR h_address4 LIKE '%");
				buf.append(search);
				buf.append("%' OR h_title LIKE '%");
				buf.append(search);
				buf.append("%') ");
				bf = true;
			}
			
			if (dateStart != null && dateEnd != null && dateStart.length() > 0 && dateEnd.length() > 0 && !dateStart.equals("체크인") && !dateEnd.equals("체크아웃")) {
				if (bf) buf.append("AND !(");
				else buf.append("WHERE !(");
				
				Calendar reg = Calendar.getInstance();
				reg.set(Integer.parseInt(dateStart.substring(0, 4)), Integer.parseInt(dateStart.substring(5,7))-1, Integer.parseInt(dateStart.substring(8)));
				SimpleDateFormat sfm = new SimpleDateFormat("yyyy-MM-dd");
				
				while (!sfm.format(reg.getTime()).equals(dateEnd)) {
					buf.append("h_res_not_possible_date LIKE '%");
					buf.append(sfm.format(reg.getTime()));
					buf.append("%' OR ");
					reg.add(Calendar.DATE, 1);
				}
				buf.append("h_res_not_possible_date LIKE '%");
				buf.append(dateEnd);
				buf.append("%') OR h_res_not_possible_date is null ");
				
				bf = true;
			}
			
			if (type1 != null && type1.length() > 0 && !type1.equals("유형")) {
				if (bf) buf.append("AND (");
				else buf.append("WHERE (");
				buf.append("h_type = '");
				buf.append(type1);
				buf.append("') ");
				bf = true;
			}
			
			if (guests != null && guests.length() > 0 && !guests.equals("인원")) {
				if (bf) buf.append("AND (");
				else buf.append("WHERE (");
				buf.append("h_guests >= ");
				buf.append(guests);
				buf.append(") ");
				bf = true;
			}
			
			if (price != null && price.length() > 0) {
				if (bf) buf.append("AND (");
				else buf.append("WHERE (");
				buf.append("h_price <= ");
				buf.append(price);
				buf.append(") ");
				bf = true;
			}
			
			buf.append(" order by h_star desc limit ?, ?");
			
			pstmt = conn.prepareStatement(buf.toString());
			pstmt.setInt(1, (start-1)*cut);
			pstmt.setInt(2, cut);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				Hotel tmp = new Hotel();
				tmp.setH_idx(rs.getInt("h_idx"));
				tmp.setH_address1(rs.getString("h_address1"));
				tmp.setH_address2(rs.getString("h_address2"));
				tmp.setH_type(rs.getString("h_type"));
				tmp.setH_type2(rs.getString("h_type2"));
				tmp.setH_star(rs.getFloat("h_star"));
				tmp.setH_title(rs.getString("h_title"));
				tmp.setH_guests(rs.getInt("h_guests"));
				tmp.setH_mapX(rs.getDouble("h_mapX"));
				tmp.setH_mapY(rs.getDouble("h_mapY"));
				tmp.setH_facilities(rs.getString("h_facilities"));
				list.add(tmp);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	// 페이지처리를 위한 검색 결과 수 methdo
			public int getHotelCount(String search, String dateStart, String dateEnd, String guests, String type1, String price){
				int result=0;
				try {
					sql = "SELECT count('h_idx') FROM hotelinfo ";
					StringBuffer buf = new StringBuffer(sql);
					boolean bf = false;
					
					if (search != null && search.length() > 0) {
						buf.append("WHERE (h_address1 LIKE '%");
						buf.append(search);
						buf.append("%' OR h_address2 LIKE '%");
						buf.append(search);
						buf.append("%' OR h_address3 LIKE '%");
						buf.append(search);
						buf.append("%' OR h_address4 LIKE '%");
						buf.append(search);
						buf.append("%' OR h_title LIKE '%");
						buf.append(search);
						buf.append("%') ");
						bf = true;
					}
					
					if (dateStart != null && dateEnd != null && dateStart.length() > 0 && dateEnd.length() > 0 && !dateStart.equals("체크인") && !dateEnd.equals("체크아웃")) {
						if (bf) buf.append("AND !(");
						else buf.append("WHERE !(");
						
						Calendar reg = Calendar.getInstance();
						reg.set(Integer.parseInt(dateStart.substring(0, 4)), Integer.parseInt(dateStart.substring(5,7))-1, Integer.parseInt(dateStart.substring(8)));
						SimpleDateFormat sfm = new SimpleDateFormat("yyyy-MM-dd");
						
						while (!sfm.format(reg.getTime()).equals(dateEnd)) {
							buf.append("h_res_not_possible_date LIKE '%");
							buf.append(sfm.format(reg.getTime()));
							buf.append("%' OR ");
							reg.add(Calendar.DATE, 1);
						}
						buf.append("h_res_not_possible_date LIKE '%");
						buf.append(dateEnd);
						buf.append("%') OR h_res_not_possible_date is null ");
						
						
						bf = true;
					}
					
					if (type1 != null && type1.length() > 0 && !type1.equals("유형")) {
						if (bf) buf.append("AND (");
						else buf.append("WHERE (");
						buf.append("h_type = '");
						buf.append(type1);
						buf.append("') ");
						bf = true;
					}
					
					if (guests != null && guests.length() > 0 && !guests.equals("인원")) {
						if (bf) buf.append("AND (");
						else buf.append("WHERE (");
						buf.append("h_guests >= ");
						buf.append(guests);
						buf.append(") ");
						bf = true;
					}
					
					if (price != null && price.length() > 0) {
						if (bf) buf.append("AND (");
						else buf.append("WHERE (");
						buf.append("h_price <= ");
						buf.append(price);
						buf.append(") ");
						bf = true;
					}
					
					pstmt = conn.prepareStatement(buf.toString());
					rs = pstmt.executeQuery();
					
					if (rs.next()) {
						result = rs.getInt("count('h_idx')");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				return result;
			}
			
	//-----------------------관리자 검색용 --------------------------------
			public int CountHotel(String search, String sel) {
				int result = 0;
				try {
					sql = "select count(h_idx) from hotelinfo ";
					StringBuffer buf = new StringBuffer(sql);
					
					if (!(search==null) && !search.equals("")) {
						if (!sel.equals("null")) {
							buf.append("where ");
							buf.append(sel);
							if (sel.equals("h_star") || sel.equals("h_price")) {buf.append(" >= ");}
							else if (sel.equals("h_date") || sel.equals("h_title") || sel.equals("h_id")) {buf.append(" LIKE ");}
							else buf.append(" = ");
							if (sel.equals("h_date") || sel.equals("h_title") || sel.equals("h_id")) {buf.append("'%");}
							if (sel.equals("h_grant")) {buf.append(search.equals("Y")?1:0);}
							else {buf.append(search);}
							if (sel.equals("h_date") || sel.equals("h_title") || sel.equals("h_id")) {buf.append("%'");}
						}
					}
					
					pstmt = conn.prepareStatement(buf.toString());
					rs = pstmt.executeQuery();
					
					if(rs.next()) result = rs.getInt(1);
				}catch(SQLException e) {
					e.printStackTrace();
				}
				return result;
			}
			
			public ArrayList<Hotel> getlist(String search, String sel, int start, int cut){
				ArrayList<Hotel> list = new ArrayList<Hotel>();
				try {
					sql = "select h_idx, h_id, h_title, h_type, h_type2, h_star, h_grant, h_date, h_price "
							+ "from hotelinfo ";
					StringBuffer buf = new StringBuffer(sql);
					
					if (!(search==null) && !search.equals("")) {
						if (!sel.equals("null")) {
							buf.append("where ");
							buf.append(sel);
							if (sel.equals("h_star") || sel.equals("h_price")) {buf.append(" >= ");}
							else if (sel.equals("h_date") || sel.equals("h_title") || sel.equals("h_id")) {buf.append(" LIKE ");}
							else buf.append(" = ");
							if (sel.equals("h_date") || sel.equals("h_title") || sel.equals("h_id")) {buf.append("'%");}
							if (sel.equals("h_grant")) {buf.append(search.equals("Y")?1:0);}
							else {buf.append(search);}
							if (sel.equals("h_date") || sel.equals("h_title") || sel.equals("h_id")) {buf.append("%'");}
						}
					}
					
					buf.append(" limit ?, ?");
					
					pstmt = conn.prepareStatement(buf.toString());
					pstmt.setInt(1, (start-1)*cut);
					pstmt.setInt(2, cut);
					rs = pstmt.executeQuery();
					
					SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
					
					while(rs.next()) {
						Hotel tmp = new Hotel();
						tmp.setH_idx(rs.getInt("h_idx"));
						tmp.setH_id(rs.getString("h_id"));
						tmp.setH_title(rs.getString("h_title"));
						tmp.setH_type(rs.getString("h_type"));
						tmp.setH_type2(rs.getString("h_type2"));
						tmp.setH_star(rs.getFloat("h_star"));
						tmp.setH_price(rs.getInt("h_price"));
						tmp.setH_date(dfm.format(rs.getDate("h_date")));
						tmp.setH_grant(rs.getInt("h_grant")>0?true:false);
						list.add(tmp);
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				return list;
			}
			
			public void DelHotel(int h_idx) {
				try {
					sql = "delete from hotelinfo where h_idx=?";
					pstmt = conn.prepareStatement(sql);
					pstmt.setInt(1, h_idx);
					pstmt.executeUpdate();
					System.out.print(h_idx +"번 삭제됨");
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
			
			public void PermitHotel(int h_idx) {
				try {
					sql = "update hotelinfo set h_grant=1 where h_idx=?";
					pstmt = conn.prepareStatement(sql);
					pstmt.setInt(1, h_idx);
					pstmt.executeUpdate();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
			//유저가등록한 호텔 갯수 가져오기
			public int CountHotel(String u_id) {
				sql = "select Count(h_idx) from hotelinfo where h_id=?";
				try {
					pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, u_id);
					rs = pstmt.executeQuery();
					if(rs.next()) {
						return rs.getInt(1);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
				
			}

			public List<Hotel> pagingHotelList(String u_id, int pageNum) {
				String sql="select * from hotelinfo where h_id=? LIMIT ?,5";
				List<Hotel> list = new ArrayList<Hotel>();
				ResultSet rs= null;
				
				try {
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, u_id);
					
					pstmt.setInt(2, (pageNum-1)*5);
				
					rs = pstmt.executeQuery();
		
					while(rs.next()) {
						Hotel hotel = new Hotel();
						hotel.setH_id(rs.getString("h_id"));
						hotel.setH_address1(rs.getString("h_address1"));
						hotel.setH_address2(rs.getString("h_address2"));
						hotel.setH_address3(rs.getString("h_address3"));
						hotel.setH_address4(rs.getString("h_address4"));
						hotel.setH_date(rs.getString("h_date"));
						hotel.setH_facilities(rs.getString("h_facilities"));
						hotel.setH_grant(rs.getBoolean("h_grant"));
						hotel.setH_guests(rs.getInt("h_guests"));
						hotel.setH_introduce(rs.getString("h_introduce"));
						hotel.setH_other(rs.getString("h_other"));
						hotel.setH_price(rs.getInt("h_price"));
						//hotel.setH_potos(rs.getString("h_potos"));
						hotel.setH_res_not_possible_date(rs.getString("h_res_not_possible_date"));
						hotel.setH_rules(rs.getString("h_rules"));
						hotel.setH_star(rs.getFloat("h_star"));
						hotel.setH_title(rs.getString("h_title"));
						hotel.setH_type(rs.getString("h_type"));
						hotel.setH_type2(rs.getString("h_type2"));
						hotel.setH_zipcode(rs.getString("h_zipcode"));
						hotel.setH_idx(rs.getInt("h_idx"));
						hotel.setH_mapX(rs.getDouble("h_mapX"));
						hotel.setH_mapY(rs.getDouble("h_mapY"));
						list.add(hotel);
					}
				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return list;
			}

			public void updatePossible(String reseDate,int h_idx,String res) { // 예약시 차단날짜 추가
				sql ="update hotelinfo set h_res_not_possible_date=? where h_idx =?";
				try {
					pstmt = conn.prepareStatement(sql);
					String updateRes = res +","+ reseDate;
					System.out.println("디비에있떤 차단날짜 : "+res);
					System.out.println("예약으로 차단날짜 : "+reseDate);
					System.out.println(updateRes + "예약 차단 날짜");
					pstmt.setString(1, updateRes);
					pstmt.setInt(2, h_idx);
					pstmt.executeUpdate();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
}
