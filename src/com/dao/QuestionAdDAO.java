package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.dto.Notice;
import com.dto.Question;

public class QuestionAdDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static QuestionAdDAO instance;
	
	public QuestionAdDAO() {
		conn = ConnectionDAO.getConnection();
	}
	public static QuestionAdDAO getInstance() {
		if(instance == null) {
			instance = new QuestionAdDAO();
		}
		return instance;
	}
	private void close(PreparedStatement pstmt, ResultSet rs) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Question> getQuestion(String pageNum, String search, String catg){
		ArrayList<Question> questionList = new ArrayList<Question>();
		try {
			if (catg.equals("q_idx")) {
				sql = "SELECT * FROM question WHERE q_idx LIKE '%"+search+"%' ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {			
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				}
			} else if (catg.equals("u_id")) {
				sql = "SELECT * FROM question WHERE u_id LIKE'%"+search+"%' ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				}
			} else if (catg.equals("q_title")) {
				sql = "SELECT * FROM question WHERE q_title LIKE '%"+search+"%' ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				}
			} else if (catg.equals("n_date")) {
				sql = "SELECT * FROM question WHERE n_date LIKE '%"+search+"%' ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				}
			} else if(catg.equals("ans") && search.equals("Y")) {
				sql = "SELECT * FROM question WHERE q_answer IS NOT NULL ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				}
			}else if(catg.equals("ans") && search.equals("N")) {
				sql = "SELECT * FROM question WHERE q_answer IS NULL ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				}
			}else if(catg.equals("") && search != ""){
				sql = "SELECT * FROM question WHERE q_idx LIKE '%" + search + "%' OR u_id LIKE'%" + search+ "%' OR q_title LIKE'%" + search + "%' OR q_date LIKE'%" + search + "%'ORDER BY q_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				} 
			}else {
				sql = "SELECT * FROM question ORDER BY q_idx DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,  (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Question tmp = new Question();
					tmp.setQ_idx(rs.getInt("q_idx"));
					tmp.setU_id(rs.getString("u_id"));
					tmp.setQ_title(rs.getString("q_title"));
					tmp.setQ_answer(rs.getString("q_answer"));
					tmp.setQ_date(rs.getString("q_date"));
					questionList.add(tmp);
				} 				
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}		
		return questionList;
	}
	
	public int targetPage(String pageNum, String search, String catg) {
		try {
			if (catg.equals("q_idx")) {
				sql = "SELECT COUNT(q_idx) FROM question WHERE q_idx LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if (catg.equals("u_id")) {
				sql = "SELECT COUNT(q_idx) FROM question WHERE u_id LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if (catg.equals("q_title")) {
				sql = "SELECT COUNT(q_idx) FROM question WHERE q_title LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if (catg.equals("n_date")) {
				sql = "SELECT COUNT(q_idx) FROM question WHERE n_date LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if(catg.equals("ans") && search.equals("Y")) {
				sql ="SELECT COUNT(q_idx) FROM question WHERE q_answer IS NOT NULL";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}				
			} else if(catg.equals("ans") && search.equals("N")) {
				sql = "SELECT COUNT(q_idx) FROM question WHERE q_answer IS NULL";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}				
			} else if(catg.equals("") && search != ""){
				sql = "SELECT COUNT(q_idx) FROM question WHERE q_idx LIKE '%" + search + "%' OR u_id LIKE'%" + search+ "%' OR q_title LIKE'%" + search + "%' OR q_date LIKE'%" + search + "%' ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else {
				sql = "SELECT COUNT(q_idx) FROM question ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}		
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}	
		return 0;
	}
	
	public Question viewQuestion(int q_idx) {
		Question questionView = new Question();
		try {
			sql = "SELECT * FROM question WHERE q_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, q_idx);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				questionView.setQ_idx(rs.getInt("q_idx"));
				questionView.setU_id(rs.getString("u_id"));
				questionView.setQ_title(rs.getString("q_title"));
				questionView.setQ_content(rs.getString("q_content"));
				questionView.setQ_date(rs.getString("q_date"));
				questionView.setQ_answer(rs.getString("q_answer"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
		return questionView;
	}
	
	public void answerQuestion(Question question) {
		try {
			sql = "UPDATE question SET q_answer = ? WHERE q_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, question.getQ_answer());
			pstmt.setInt(2, question.getQ_idx());
			pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
