package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dto.User;

public class UserDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static UserDAO instance;
	
	private UserDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static UserDAO getInstance() {
		if (instance == null) instance = new UserDAO();
		return instance;
	}
	private void close(PreparedStatement pstmt, ResultSet rs) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	public User getUser(String u_id) {
		User user = new User();
		try {
			sql = "SELECT * FROM userprivacy WHERE u_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u_id);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				user.setU_id(rs.getString("u_id"));
				user.setU_potoaddress(rs.getString("u_potoaddress"));
				user.setU_first_name(rs.getString("u_first_name"));
				user.setU_last_name(rs.getString("u_last_name"));
				user.setActivate(rs.getInt("u_activation")==1?true:false);
				user.setU_date(rs.getString("u_date"));
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
		
		return user;
	}
	public User login(String id, String pwd) {
		User user = null;
		try {
			sql = "SELECT a_id FROM adminlist WHERE a_id = ? AND a_pass = password(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			rs = pstmt.executeQuery();	
			if(rs.next()) {
				user = new User();
				user.setU_id(id);
				user.setU_pwd(pwd);
				user.setAdmin(true);
			}
			
			sql = "SELECT u_first_name, u_last_name, u_company, u_introduce, u_residence, u_activation, u_date FROM userprivacy where u_id = ? AND u_pwd = password(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			rs = pstmt.executeQuery();		
			if(rs.next()) {
				user = new User();
				user.setU_id(id);
				user.setU_pwd(pwd);
				user.setAdmin(false);
				user.setU_first_name(rs.getString("u_first_name"));
				user.setU_last_name(rs.getString("u_last_name"));
				user.setU_company(rs.getString("u_company"));
				user.setU_introduce(rs.getString("u_introduce"));
				user.setU_residence(rs.getString("u_residence"));
				user.setActivate(rs.getInt("u_activation")==1?true:false);
				user.setU_date(rs.getString("u_date"));
			}
			if (user != null && !user.isAdmin()) {
				sql = "UPDATE userprivacy SET u_login = CURRENT_TIMESTAMP() WHERE u_id =?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
				pstmt.executeUpdate();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			this.close(pstmt, rs);
		}
		return user;
	}
	
	
	public void joinus(String ID, String fName, String lName, String password) {
		try {
			sql = "INSERT INTO userprivacy(u_id, u_pwd, u_last_name, u_first_name) values (?, password(?), ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, ID);
			pstmt.setString(2, password);
			pstmt.setString(3, lName);
			pstmt.setString(4, fName);
			pstmt.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<User> getUserList(String pageNum, String search, String catg){
		ArrayList<User> userList = new ArrayList<User>();
		
		try {
			sql ="UPDATE userprivacy SET u_activation = FALSE WHERE u_login <= DATE_ADD(CURRENT_DATE(), INTERVAL -3 MONTH)";
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			
			if(catg.equals("id")) {
				sql = "SELECT * FROM userprivacy WHERE u_id LIKE '%"+search+"%' ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			} else if(catg.equals("name")) {
				sql = "SELECT * FROM userprivacy WHERE u_last_name LIKE '%"+search+"%' ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			} else if(catg.equals("date")) {
				sql = "SELECT * FROM userprivacy WHERE u_date LIKE '%"+search+"%' ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			} else if(catg.equals("activate") && search.equals("Y")) {
				sql = "SELECT * FROM userprivacy WHERE u_activation LIKE TRUE ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			}else if(catg.equals("activate") && search.equals("N")) {
				sql = "SELECT * FROM userprivacy WHERE u_activation LIKE 'false' ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			}else if(catg.equals("") && search != "") {
				sql = "SELECT * FROM userprivacy WHERE u_id LIKE '%"+search+"%' OR u_last_name LIKE '%"+search+"%' OR u_date LIKE '%"+search+"%' ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			}else {
				sql = "SELECT * FROM userprivacy ORDER BY u_date DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,(Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					User user = new User();
					user.setU_id(rs.getString("u_id"));
					user.setU_potoaddress(rs.getString("u_potoaddress"));
					user.setU_first_name(rs.getString("u_first_name"));
					user.setU_last_name(rs.getString("u_last_name"));
					user.setActivate(rs.getInt("u_activation")==1?true:false);
					user.setU_date(rs.getString("u_date"));
					userList.add(user);
				}
			}			
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}		
		return userList;
	}
	
	public int targetPage(String pageNum, String search, String catg) {
		try {
			if(catg.equals("id")) {
				sql = "SELECT COUNT(u_id) FROM userprivacy WHERE u_id LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if(catg.equals("name")) {
				sql = "SELECT COUNT(u_id) FROM userprivacy WHERE u_last_name LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if(catg.equals("date")) {
				sql = "SELECT COUNT(u_id) FROM userprivacy WHERE u_date LIKE '%"+search+"%' ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if(catg.equals("activate") && search.equals("Y")) {
				sql = "SELECT COUNT(u_id) FROM userprivacy WHERE u_activation LIKE TRUE";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("activate") && search.equals("N")) {
				sql = "SELECT COUNT(u_id) FROM userprivacy WHERE u_activation LIKE false ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else if(catg.equals("") && search != "") {
				sql = "SELECT COUNT(u_id) FROM userprivacy WHERE u_id LIKE '%"+search+"%' OR u_last_name LIKE '%"+search+"%' OR u_activaion LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}else {
				sql = "SELECT COUNT(u_id) FROM userprivacy ";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}			
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}	
		return 0;
	}
	
	public void delUser(User user) {
		try {
			sql = "DELETE FROM userprivacy WHERE u_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,user.getU_id());
			pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
	}
	
	   public void UpdateUser(User user) {
		      try {
		         sql = "Update userprivacy set u_first_name=?, u_last_name=?,u_residence=?, u_phone=?  where u_id=?";
		         pstmt = conn.prepareStatement(sql);
		         pstmt.setString(1, user.getU_first_name());
		         pstmt.setString(2, user.getU_last_name());
		         pstmt.setString(3, user.getU_residence());
		         pstmt.setString(4, user.getU_phone());
		         pstmt.setString(5, user.getU_id());
		         pstmt.executeUpdate();
		      }catch(SQLException e) {
		         e.printStackTrace();
		      }
		   }
}
