package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HoteltypeDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static HoteltypeDAO instance;
	
	private HoteltypeDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static HoteltypeDAO getInstance() {
		if (instance == null) instance = new HoteltypeDAO();
		return instance;
	}
	
	public void DelType1(String type) {
		try {
			sql = "DELETE FROM hoteltype1 WHERE h_type=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, type);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void DelType2(String type) {
		try {
			sql = "DELETE FROM hoteltype2 WHERE h_type=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, type);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> GetType1List(){
		ArrayList<String> list= new ArrayList<String>();
		try {
			sql = "SELECT h_type from hoteltype1";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				list.add(rs.getString("h_type"));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<String> GetType2List(){
		ArrayList<String> list= new ArrayList<String>();
		try {
			sql = "SELECT h_type from hoteltype2";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				list.add(rs.getString("h_type"));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void SetType1(String type) {
		try {
			sql = "INSERT INTO hoteltype1 VALUES(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, type);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void SetType2(String type) {
		try {
			sql = "INSERT INTO hoteltype2 VALUES(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, type);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
