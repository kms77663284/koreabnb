package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dto.Notice;

public class NoticeDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static NoticeDAO instance;

	public NoticeDAO() {
		conn = ConnectionDAO.getConnection();
	}

	public static NoticeDAO getInstance() {
		if (instance == null)
			instance = new NoticeDAO();
		return instance;
	}
	private void close(PreparedStatement pstmt, ResultSet rs) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Notice> getNotice(String pageNum) {
		ArrayList<Notice> noticeList = new ArrayList<Notice>();
		try {
			sql = "SELECT * FROM notice WHERE n_idx > (SELECT MAX(n_idx) FROM notice) - ? AND n_idx <= (SELECT MAX(n_idx) FROM notice) -? ORDER BY n_idx DESC";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(pageNum) * 10);
			pstmt.setInt(2, (Integer.parseInt(pageNum) - 1) * 10);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Notice tmp = new Notice();
				tmp.setN_idx(rs.getInt("n_idx"));
				tmp.setN_title(rs.getString("n_title"));
				tmp.setN_vcount(rs.getInt("n_vcount"));
				tmp.setA_id(rs.getString("a_id"));
				tmp.setN_date(rs.getString("n_date"));
				noticeList.add(tmp);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}

		return noticeList;
	}

	public int targetPage(String pageNum) {
		try {
			sql = "SELECT COUNT(n_idx) FROM notice WHERE n_idx > ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, (Integer.parseInt(pageNum) - 1) * 10);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(1) / 10;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
		return 0;
	}
	
	public ArrayList<Notice> searchNotice(String pageNum, String search, String catg) {
		ArrayList<Notice> noticeList = new ArrayList<Notice>();

		try {
			if (catg.equals("n_idx")) {
				sql = "SELECT * FROM notice WHERE n_idx LIKE '%"+search+"%' ORDER BY n_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Notice tmp = new Notice();
					tmp.setN_idx(rs.getInt("n_idx"));
					tmp.setN_title(rs.getString("n_title"));
					tmp.setN_vcount(rs.getInt("n_vcount"));
					tmp.setA_id(rs.getString("a_id"));
					tmp.setN_date(rs.getString("n_date"));
					noticeList.add(tmp);
				}
			} else if (catg.equals("n_title")) {
				sql = "SELECT * FROM notice WHERE n_title LIKE'%"+search+"%' ORDER BY n_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Notice tmp = new Notice();
					tmp.setN_idx(rs.getInt("n_idx"));
					tmp.setN_title(rs.getString("n_title"));
					tmp.setN_vcount(rs.getInt("n_vcount"));
					tmp.setA_id(rs.getString("a_id"));
					tmp.setN_date(rs.getString("n_date"));
					noticeList.add(tmp);
				}
			} else if (catg.equals("a_id")) {
				sql = "SELECT * FROM notice WHERE a_id LIKE '%"+search+"%' ORDER BY n_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Notice tmp = new Notice();
					tmp.setN_idx(rs.getInt("n_idx"));
					tmp.setN_title(rs.getString("n_title"));
					tmp.setN_vcount(rs.getInt("n_vcount"));
					tmp.setA_id(rs.getString("a_id"));
					tmp.setN_date(rs.getString("n_date"));
					noticeList.add(tmp);
				}
			} else if (catg.equals("n_date")) {
				sql = "SELECT * FROM notice WHERE n_date LIKE '%"+search+"%' ORDER BY n_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Notice tmp = new Notice();
					tmp.setN_idx(rs.getInt("n_idx"));
					tmp.setN_title(rs.getString("n_title"));
					tmp.setN_vcount(rs.getInt("n_vcount"));
					tmp.setA_id(rs.getString("a_id"));
					tmp.setN_date(rs.getString("n_date"));
					noticeList.add(tmp);
				}
			} else if(catg.equals("") && search != ""){
				sql = "SELECT * FROM notice WHERE n_idx LIKE '%" + search + "%' OR n_title LIKE'%" + search+ "%' OR a_id LIKE'%" + search + "%' OR n_date LIKE'%" + search + "%' ORDER BY n_idx DESC LIMIT ?, 10 ";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Notice tmp = new Notice();
					tmp.setN_idx(rs.getInt("n_idx"));
					tmp.setN_title(rs.getString("n_title"));
					tmp.setN_vcount(rs.getInt("n_vcount"));
					tmp.setA_id(rs.getString("a_id"));
					tmp.setN_date(rs.getString("n_date"));
					noticeList.add(tmp);
				} 
			}else {
				sql = "SELECT * FROM notice ORDER BY n_idx DESC LIMIT ?, 10";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1,  (Integer.parseInt(pageNum)-1)*10);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Notice tmp = new Notice();
					tmp.setN_idx(rs.getInt("n_idx"));
					tmp.setN_title(rs.getString("n_title"));
					tmp.setN_vcount(rs.getInt("n_vcount"));
					tmp.setA_id(rs.getString("a_id"));
					tmp.setN_date(rs.getString("n_date"));
					noticeList.add(tmp);
				} 				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
		return noticeList;
	}

	public int targetPage_search(String pageNum, String search, String catg) {
		try {			
			if (catg.equals("n_idx")) {
				sql = "SELECT COUNT(n_idx) FROM notice WHERE n_idx LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if (catg.equals("n_title")) {
				sql = "SELECT COUNT(n_idx) FROM notice WHERE n_title LIKE'%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if (catg.equals("a_id")) {
				sql = "SELECT COUNT(n_idx) FROM notice WHERE a_id LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if (catg.equals("n_date")) {
				sql = "SELECT COUNT(n_idx) FROM notice WHERE n_date LIKE '%"+search+"%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else if(catg.equals("") && search != ""){
				sql = "SELECT COUNT(n_idx) FROM notice WHERE n_idx LIKE '%" + search + "%' OR n_title LIKE'%" + search + "%' OR a_id LIKE'%" + search + "%' OR n_date LIKE'%" + search + "%'";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			} else{
				sql = "SELECT COUNT(n_idx) FROM notice";
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					return rs.getInt(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
		return 0;
	}
	
	public void addNotice(Notice notice) {
		try {
			sql = "INSERT INTO notice (a_id, n_title, n_content) VALUES(?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, notice.getA_id());
			pstmt.setString(2, notice.getN_title());
			pstmt.setString(3, notice.getN_content());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
	}

	public Notice viewNotice(int n_idx) {
		Notice noticeView = new Notice();
		try {
			sql = "UPDATE notice set n_vcount = n_vcount +1 WHERE n_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, n_idx);
			pstmt.executeUpdate();

			sql = "SELECT * FROM notice WHERE n_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, n_idx);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				noticeView.setN_idx(rs.getInt("n_idx"));
				noticeView.setA_id(rs.getString("a_id"));
				noticeView.setN_title(rs.getString("n_title"));
				noticeView.setN_content(rs.getString("n_content"));
				noticeView.setN_vcount(rs.getInt("n_vcount"));
				noticeView.setN_date(rs.getString("n_date"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
		return noticeView;
	}

	public void delNotice(Notice notice) {
		try {
			sql = "DELETE FROM notice WHERE n_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, notice.getN_idx());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
	}

	public void modNotice(Notice notice) {
		try {
			sql = "UPDATE notice SET n_title = ?, n_content = ? WHERE n_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, notice.getN_title());
			pstmt.setString(2, notice.getN_content());
			pstmt.setInt(3, notice.getN_idx());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close(pstmt, rs);
		}
	}
}
