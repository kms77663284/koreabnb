package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dto.Review;

public class ReviewDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static ReviewDAO instance;
	
	private ReviewDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static ReviewDAO getInstance() {
		if (instance == null) instance = new ReviewDAO();
		return instance;
	}
	
	public ArrayList<Review> getlist(String search, String sel){
		ArrayList<Review> list = new ArrayList<Review>();
		try {
			sql = "select r_idx, u_id, h_idx, r_content, r_date, r_star from review ";
			StringBuffer buf = new StringBuffer(sql);
			
			if (!search.equals("")) {
				if (!sel.equals("null")) {
					buf.append(sel);
					buf.append("=");
					buf.append(search);
				}
			}
			
			pstmt = conn.prepareStatement(buf.toString());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void delReview(String rid) {
		try {
			sql = "select h_idx from review where r_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(rid));
			rs = pstmt.executeQuery();
			
			rs.next();
			int h_idx = rs.getInt(1);
			
			sql = "delete from review where r_idx = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(rid));
			pstmt.executeUpdate();
			
			sql = "select count(r_star) from review where h_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, h_idx);
			rs = pstmt.executeQuery();
			
			rs.next();
			int cnt = rs.getInt(1);
			
			if (cnt == 0) {
				sql = "update hotelinfo set h_star=0 where h_idx=?";
				pstmt.setInt(1, h_idx);
				pstmt.executeUpdate();
			}else {
				sql = "select avg(r_star) from review where h_idx=?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, h_idx);
				rs = pstmt.executeQuery();
				
				rs.next();
				double avg_star = rs.getDouble(1);
				
				sql = "update hotelinfo set h_star=? where h_idx=?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setDouble(1, avg_star);
				pstmt.setInt(2, h_idx);
				pstmt.executeUpdate();
			}
			rs.close();
			pstmt.close();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Review> getReviews(String h_idx){
		ArrayList<Review> list = new ArrayList<Review>();
		try {
			sql = "select * from review where h_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, h_idx);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Review review = new Review();
				review.setR_uid(rs.getString("u_id"));
				review.setR_content(rs.getString("r_content"));
				review.setR_date(rs.getString("r_date"));
				review.setR_idx(rs.getInt("r_idx"));
				review.setR_hidx(rs.getInt("h_idx"));
				review.setR_star(rs.getFloat("r_star"));
				list.add(review);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	public int CountReview(String search, String sel) {
		int result = 0;
		try {
			sql = "select count(r_idx) from review ";
			StringBuffer buf = new StringBuffer(sql);
			
			if (!(search==null) && !search.equals("")) {
				if (!sel.equals("null")) {
					buf.append("where ");
					buf.append(sel);
					if (sel.equals("r_star")) {buf.append(" >= ");}
					else if (sel.equals("r_date") || sel.equals("r_content")) {buf.append(" LIKE ");}
					else buf.append(" = ");
					if (sel.equals("r_date") || sel.equals("r_content")) {buf.append("'%");}
					buf.append(search);
					if (sel.equals("r_date") || sel.equals("r_content")) {buf.append("%'");}
				}
			}
			
			pstmt = conn.prepareStatement(buf.toString());
			rs = pstmt.executeQuery();
			
			if(rs.next()) result = rs.getInt(1);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<Review> getlist(String search, String sel, int start, int cut){
		ArrayList<Review> list = new ArrayList<Review>();
		try {
			sql = "select r_idx, u_id, h_idx, r_content, r_date, r_star from review ";
			StringBuffer buf = new StringBuffer(sql);
			
			if (!(search==null) && !search.equals("")) {
				if (!sel.equals("null")) {
					buf.append("where ");
					buf.append(sel);
					if (sel.equals("r_star")) {buf.append(" >= ");}
					else if (sel.equals("r_date") || sel.equals("r_content")) {buf.append(" LIKE ");}
					else buf.append(" = ");
					if (sel.equals("r_date") || sel.equals("r_content")) {buf.append("'%");}
					buf.append(search);
					if (sel.equals("r_date") || sel.equals("r_content")) {buf.append("%'");}
				}
			}
			
			buf.append(" limit ?, ?");
			
			pstmt = conn.prepareStatement(buf.toString());
			pstmt.setInt(1, (start-1)*cut);
			pstmt.setInt(2, cut);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				Review tmp = new Review();
				tmp.setR_date(rs.getString("r_date"));
				tmp.setR_content(rs.getString("r_content"));
				tmp.setR_hidx(rs.getInt("h_idx"));
				tmp.setR_idx(rs.getInt("r_idx"));
				tmp.setR_uid(rs.getString("u_id"));
				tmp.setR_star(rs.getDouble("r_star"));
				list.add(tmp);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void addReview(Review rv) {
		try {
			sql = "select r_idx from review where u_id=? and h_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,rv.getR_uid());
			pstmt.setInt(2,  rv.getR_hidx());
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				sql = "update review set r_content=?, r_star=?, r_date=CURRENT_TIMESTAMP()  where u_id=? and h_idx=?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1,rv.getR_content());
				pstmt.setDouble(2, rv.getR_star());
				pstmt.setString(3,rv.getR_uid());
				pstmt.setInt(4, rv.getR_hidx());
				pstmt.executeUpdate();
			}else {
				sql = "insert into review(u_id, h_idx, r_content, r_star) values(?,?,?,?)";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1,rv.getR_uid());
				pstmt.setInt(2,  rv.getR_hidx());
				pstmt.setString(3,rv.getR_content());
				pstmt.setDouble(4,  rv.getR_star());
				pstmt.executeUpdate();
			}
			sql = "select avg(r_star) from review where h_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, rv.getR_hidx());
			rs = pstmt.executeQuery();
			
			double avg_star = 0;
			if (rs.next()) {
				avg_star = rs.getFloat(1);
			}
			
			sql = "update hotelinfo set h_star=? where h_idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1, avg_star);
			pstmt.setInt(2, rv.getR_hidx());
			pstmt.executeUpdate();
			
			rs.close();
			pstmt.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	public Review selectReview(int h_idx, String u_id) {
		Review r = new Review();
		try {
			sql = "select * from review where h_idx =? and u_id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, h_idx);
			pstmt.setString(2, u_id);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				r.setR_uid(rs.getString("u_id"));
				r.setR_hidx(rs.getInt("h_idx"));
				r.setR_idx(rs.getInt("r_idx"));
				r.setR_star(rs.getFloat("r_star"));
				r.setR_content(rs.getString("r_content"));
				r.setR_date(rs.getString("r_date"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return r;
	}
	public Boolean checkReview(int h_idx, String u_id) {
		try {
			sql = "select r_idx from review where h_idx =? and u_id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, h_idx);
			pstmt.setString(2, u_id);
			
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
