package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FacilitiesDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static FacilitiesDAO instance;
	
	private FacilitiesDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static FacilitiesDAO getInstance() {
		if (instance == null) instance = new FacilitiesDAO();
		return instance;
	}
	
	public void AddFacilities(String fac) {
		try {
			sql = "INSERT INTO Facilities VALUES(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, fac);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void DelFacilities(String fac) {
		try {
			sql = "DELETE FROM Facilities WHERE f_name=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, fac);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> SelectFacilities() {
		ArrayList<String> list = new ArrayList<String>();
		try {
			sql ="select * from Facilities";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {list.add(rs.getString("f_name"));}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
}
