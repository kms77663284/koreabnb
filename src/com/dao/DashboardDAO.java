package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.dto.Count;

public class DashboardDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static DashboardDAO instance;
	
	public DashboardDAO(){
		conn = ConnectionDAO.getConnection();
	}
	
	public static DashboardDAO getInstance() {
		if (instance == null) instance = new DashboardDAO();
		return instance;
	}
	public Count dashboard() {
		Count count = new Count();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar today = Calendar.getInstance();
		String datestart =  sdf.format(today.getTime());
		today.add(today.DATE, 1);
		String dateend = sdf.format(today.getTime());
		try {
			sql = "SELECT COUNT(*) FROM userprivacy";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();			
			if(rs.next()) {
				count.setU_count(rs.getInt(1));
			}		
			
			sql = "SELECT COUNT(*) FROM hotelinfo";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();			
			if(rs.next()) {
				count.setH_count(rs.getInt(1));
			}	
			
			sql = "SELECT COUNT(*) FROM booking";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();			
			if(rs.next()) {
				count.setB_count(rs.getInt(1));
			}	
			
			sql = "SELECT COUNT(*) FROM userprivacy WHERE u_date >= ? AND u_date < ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, datestart);
			pstmt.setString(2, dateend);
			rs = pstmt.executeQuery();			
			if(rs.next()) {
				count.setU_tcount(rs.getInt(1));
			}		
			
			sql = "SELECT COUNT(*) FROM hotelinfo WHERE h_date >= ? AND h_date < ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, datestart);
			pstmt.setString(2, dateend);
			rs = pstmt.executeQuery();			
			if(rs.next()) {
				count.setH_tcount(rs.getInt(1));

			}	
			
			sql = "SELECT COUNT(*) FROM booking WHERE b_date >= ? AND b_date < ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, datestart);
			pstmt.setString(2, dateend);
			rs = pstmt.executeQuery();			
			if(rs.next()) {
				count.setB_tcount(rs.getInt(1));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {if (pstmt != null) {pstmt.close();}
			} catch (Exception e) {	e.printStackTrace();}
			try {if(rs != null) {rs.close();}
			} catch (Exception e) {e.printStackTrace();}
		}
		return count;
	}
}
