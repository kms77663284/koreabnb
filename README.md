# KOREA BNB 

**숙박 공유 서비스이다. 자신의 방이나 집, 별장 등 사람이 지낼 수 있는 모든 공간을 임대할 수 있다. **

에어비엔비를 벤치마킹하여 제작한 홈페이지 개발기간 : 2019.10.16 ~ 11. 18(32일)

<img src="https://gitlab.com/kms77663284/koreabnb/raw/master/doc/mian.gif">


## 개발환경
- JAVA 1.8
- Apache Tomcat8.5
- JSP / Servlet
- JQuery, Ajax
- Bootstrap

## 구조

<img src="https://gitlab.com/kms77663284/koreabnb/raw/master/doc/map.png">

## ERD

<img src="https://gitlab.com/kms77663284/koreabnb/raw/master/doc/ERD.png">

## 홈페이지 기능
- 사용자 : 회원가입, 로그인, 마이페이지,계정정보변경, 예약관리,숙소 예약
- 호스트 : 숙소등록, 숙소관리, 예약관리
- 관리자 : 공지사항관리, 회원관리, 숙소관리, 예약관리, 문의사항답변, 숙소 유형,종류,규칙 관리
- 공통 : 인기 숙소 노출, 숙소검색 필터링, 숙소 지도표시


## 보완점
- XSS 방어코드가 없어 취약함
- JDBC PreparedStatement를 활용한 SQL Injection 방어


